/*
 * Copyright (c) 2014 Samsung Electronics Co., Ltd All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
#include "main.h"

Evas_Object *circle_icon;
Eina_Bool toggle_circle_button = EINA_TRUE;

char *button_menu_names[] = {
	"default", "circle", "textbutton", "bottom text", "bottom icon",
	NULL
};

typedef struct _item_data {
	int index;
	Elm_Object_Item *item;
} item_data;

static void
gl_selected_cb(void *data, Evas_Object *obj, void *event_info)
{
	Elm_Object_Item *it = (Elm_Object_Item *)event_info;
	elm_genlist_item_selected_set(it, EINA_FALSE);
}

static char *
_gl_menu_title_text_get(void *data, Evas_Object *obj, const char *part)
{
	char buf[1024];

	snprintf(buf, 1023, "%s", "Button");
	return strdup(buf);
}

static char *
_gl_menu_text_get(void *data, Evas_Object *obj, const char *part)
{
	char buf[1024];
	item_data *id = (item_data *)data;
	int index = id->index;

	if (!strcmp(part, "elm.text")) {
		snprintf(buf, 1023, "%s", button_menu_names[index]);
		return strdup(buf);
	}
	return NULL;
}

static void
_gl_menu_del(void *data, Evas_Object *obj)
{
	// FIXME: Unrealized callback can be called after this.
	// Accessing Item_Data can be dangerous on unrealized callback.
	item_data *id = (item_data *)data;
	if (id) free(id);
}

static void
btn_clicked_cb(void *data, Evas_Object *obj, void *event_info)
{
	printf("clicked event on Button\n");
}

static void
circle_btn_pressed_cb(void *data, Evas_Object *obj, void *event_info)
{
	Evas_Object *ic = circle_icon;
	if (toggle_circle_button) {
		toggle_circle_button = EINA_FALSE;
		elm_image_file_set(ic, ICON_DIR "/tw_number_controller_icon_ringtone_sound.png", NULL);
	} else {
		toggle_circle_button = EINA_TRUE;
		elm_image_file_set(ic, ICON_DIR "/tw_number_controller_icon_ringtone_mute.png", NULL);
	}
	evas_object_size_hint_min_set(ic, 100, 100);
	evas_object_show(ic);
}

static Evas_Object*
create_scroller(Evas_Object *parent)
{
	Evas_Object *scroller = elm_scroller_add(parent);
	elm_scroller_bounce_set(scroller, EINA_FALSE, EINA_TRUE);
	elm_scroller_policy_set(scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
	evas_object_show(scroller);

	return scroller;
}

static Evas_Object*
create_default_button_view(Evas_Object *parent)
{
	Evas_Object *btn, *box, *ic, *layout;
	//layout box
	box = elm_box_add(parent);
	evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(box, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_box_padding_set(box, 0, 25 * elm_config_scale_get());
	evas_object_show(box);

	// text only button
	layout = elm_layout_add(box);
	elm_layout_file_set(layout, ELM_DEMO_EDJ, "button_layout_1");

	btn = elm_button_add(box);
	evas_object_smart_callback_add(btn, "clicked", btn_clicked_cb, NULL);
	evas_object_size_hint_weight_set(btn, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(btn, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_object_text_set(btn, "Button");
	evas_object_show(btn);

	elm_object_part_content_set(layout, "elm.swallow.content", btn);
	elm_box_pack_end(box, layout);
	evas_object_show(layout);

	// icon only button
	layout = elm_layout_add(box);
	elm_layout_file_set(layout, ELM_DEMO_EDJ, "button_layout_1");

	btn = elm_button_add(box);
	evas_object_smart_callback_add(btn, "clicked", btn_clicked_cb, NULL);
	evas_object_size_hint_weight_set(btn, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(btn, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_show(btn);

	ic = elm_image_add(box);
	elm_image_file_set(ic, ICON_DIR "/tw_button_ic_save.png", NULL);
	elm_object_part_content_set(btn, "elm.swallow.content", ic);
	evas_object_show(ic);

	elm_object_part_content_set(layout, "elm.swallow.content", btn);
	elm_box_pack_end(box, layout);
	evas_object_show(layout);

	// text + icon button
	layout = elm_layout_add(box);
	elm_layout_file_set(layout, ELM_DEMO_EDJ, "button_layout_2");

	btn = elm_button_add(box);
	evas_object_smart_callback_add(btn, "clicked", btn_clicked_cb, NULL);
	evas_object_size_hint_weight_set(btn, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(btn, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_object_text_set(btn, "Button");
	evas_object_show(btn);

	ic = elm_image_add(box);
	elm_image_file_set(ic, ICON_DIR "/tw_button_ic_save.png", NULL);
	elm_object_part_content_set(btn, "elm.swallow.content", ic);
	evas_object_show(ic);

	elm_object_part_content_set(layout, "elm.swallow.content", btn);
	evas_object_show(layout);
	elm_box_pack_end(box, layout);

	return box;
}

void
_default_btn_cb(void *data, Evas_Object *obj, void *event_info)
{
	appdata_s *ad = (appdata_s *)data;
	Evas_Object *scroller, *circle_scroller, *layout;
	Evas_Object *nf = ad->nf;
	Elm_Object_Item *nf_it;

	scroller = create_scroller(nf);
	layout = create_default_button_view(scroller);
	elm_object_content_set(scroller, layout);

	circle_scroller = eext_circle_object_scroller_add(scroller, ad->circle_surface);
	eext_circle_object_scroller_policy_set(circle_scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
	eext_rotary_object_event_activated_set(circle_scroller, EINA_TRUE);

	nf_it = elm_naviframe_item_push(nf, "Default Styles", NULL, NULL, scroller, NULL);
	elm_naviframe_item_title_enabled_set(nf_it, EINA_FALSE, EINA_FALSE);
}

static Evas_Object*
create_circle_button_view(Evas_Object *parent)
{
	Evas_Object *btn, *box;

	box = elm_box_add(parent);
	evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(box, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_show(box);

	btn = elm_button_add(box);
	evas_object_smart_callback_add(btn, "pressed", circle_btn_pressed_cb, NULL);
	elm_object_style_set(btn, "circle");

	circle_icon = elm_image_add(box);
	elm_image_file_set(circle_icon, ICON_DIR "/tw_number_controller_icon_ringtone_mute.png", NULL);
	evas_object_size_hint_min_set(circle_icon, 100, 100);
	evas_object_show(circle_icon);
	elm_object_part_content_set(btn, "elm.swallow.content", circle_icon);

	evas_object_show(btn);
	elm_box_pack_end(box, btn);

	return box;
}

void
_circle_btn_cb(void *data, Evas_Object *obj, void *event_info)
{
	appdata_s *ad = (appdata_s *)data;
	Evas_Object *scroller, *circle_scroller, *layout;
	Evas_Object *nf = ad->nf;
	Elm_Object_Item *nf_it;

	scroller = create_scroller(nf);
	layout = create_circle_button_view(scroller);
	elm_object_content_set(scroller, layout);

	circle_scroller = eext_circle_object_scroller_add(scroller, ad->circle_surface);
	eext_circle_object_scroller_policy_set(circle_scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
	eext_rotary_object_event_activated_set(circle_scroller, EINA_TRUE);

	nf_it = elm_naviframe_item_push(nf, "Default Styles", NULL, NULL, scroller, NULL);
	elm_naviframe_item_title_enabled_set(nf_it, EINA_FALSE, EINA_FALSE);
}

static Evas_Object*
create_text_button_view(Evas_Object *parent)
{
	Evas_Object *btn, *box;
	char buf[64];

	box = elm_box_add(parent);
	evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(box, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_show(box);

	btn = elm_button_add(box);
	evas_object_smart_callback_add(btn, "clicked", btn_clicked_cb, NULL);
	snprintf(buf, sizeof(buf), "TEXT BUTTON");
	elm_object_text_set(btn, buf);
	elm_object_style_set(btn, "textbutton");
	evas_object_show(btn);
	elm_box_pack_end(box, btn);

	return box;
}

void
_text_btn_cb(void *data, Evas_Object *obj, void *event_info)
{
	appdata_s *ad = (appdata_s *)data;
	Evas_Object *scroller, *circle_scroller, *layout;
	Evas_Object *nf = ad->nf;
	Elm_Object_Item *nf_it;

	scroller = create_scroller(nf);
	layout = create_text_button_view(scroller);
	elm_object_content_set(scroller, layout);

	circle_scroller = eext_circle_object_scroller_add(scroller, ad->circle_surface);
	eext_circle_object_scroller_policy_set(circle_scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
	eext_rotary_object_event_activated_set(circle_scroller, EINA_TRUE);

	nf_it = elm_naviframe_item_push(nf, "Default Styles", NULL, NULL, scroller, NULL);
	elm_naviframe_item_title_enabled_set(nf_it, EINA_FALSE, EINA_FALSE);
}

void
_bottom_text_btn_cb(void *data, Evas_Object *obj, void *event_info)
{
	appdata_s *ad = (appdata_s *)data;
	Evas_Object *nf = ad->nf;
	Evas_Object *btn, *ly;

	ly = elm_layout_add(nf);
	elm_layout_theme_set(ly, "layout", "bottom_button", "default");
	evas_object_show(ly);

	btn = elm_button_add(nf);
	elm_object_style_set(btn, "bottom");
	elm_object_text_set(btn, "BOTTOM BTN");
	elm_object_part_content_set(ly, "elm.swallow.button", btn);
	evas_object_smart_callback_add(btn, "clicked", btn_clicked_cb, NULL);
	evas_object_show(btn);

	elm_naviframe_item_push(nf, "Bottom", NULL, NULL, ly, NULL);
}

void
_bottom_icon_btn_cb(void *data, Evas_Object *obj, void *event_info)
{
	appdata_s *ad = (appdata_s *)data;
	Evas_Object *nf = ad->nf;
	Evas_Object *btn, *ly, *icon;

	ly = elm_layout_add(nf);
	elm_layout_theme_set(ly, "layout", "bottom_button", "default");
	evas_object_show(ly);

	btn = elm_button_add(nf);
	elm_object_style_set(btn, "bottom");
	elm_object_part_content_set(ly, "elm.swallow.button", btn);
	evas_object_smart_callback_add(btn, "clicked", btn_clicked_cb, NULL);
	evas_object_show(btn);

	icon = elm_image_add(btn);
	elm_image_file_set(icon, ICON_DIR "/tw_number_controller_icon_ringtone_mute.png", NULL);
	evas_object_size_hint_weight_set(icon, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	elm_object_part_content_set(btn, "elm.swallow.content", icon);
	evas_object_show(icon);

	elm_naviframe_item_push(nf, "Bottom", NULL, NULL, ly, NULL);
}

void
button_cb(void *data, Evas_Object *obj, void *event_info)
{
	appdata_s *ad = (appdata_s *)data;
	Evas_Object *genlist;
	Evas_Object *circle_genlist;
	Elm_Genlist_Item_Class *itc = elm_genlist_item_class_new();
	Elm_Genlist_Item_Class *ttc = elm_genlist_item_class_new();
	Elm_Genlist_Item_Class *ptc = elm_genlist_item_class_new();
	Elm_Object_Item *nf_it;
	item_data *id;
	int index = 0;

	if (!ad) return;

	genlist = elm_genlist_add(ad->nf);
	elm_genlist_mode_set(genlist, ELM_LIST_COMPRESS);
	evas_object_smart_callback_add(genlist, "selected", gl_selected_cb, NULL);

	circle_genlist = eext_circle_object_genlist_add(genlist, ad->circle_surface);
	eext_circle_object_genlist_scroller_policy_set(circle_genlist, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
	eext_rotary_object_event_activated_set(circle_genlist, EINA_TRUE);

	ttc->item_style = "title";
	ttc->func.text_get = _gl_menu_title_text_get;
	ttc->func.del = _gl_menu_del;

	itc->item_style = "default";
	itc->func.text_get = _gl_menu_text_get;
	itc->func.del = _gl_menu_del;

	ptc->item_style = "padding";
	ptc->func.del = _gl_menu_del;

	elm_genlist_item_append(genlist, ttc, NULL, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);

	id = calloc(sizeof(item_data), 1);
	if (id) {
		id->index = index++;
		id->item = elm_genlist_item_append(genlist, itc, id, NULL, ELM_GENLIST_ITEM_NONE, _default_btn_cb, ad);
	}
	id = calloc(sizeof(item_data), 1);
	if (id) {
		id->index = index++;
		id->item = elm_genlist_item_append(genlist, itc, id, NULL, ELM_GENLIST_ITEM_NONE, _circle_btn_cb, ad);
	}
	id = calloc(sizeof(item_data), 1);
	if (id) {
		id->index = index++;
		id->item = elm_genlist_item_append(genlist, itc, id, NULL, ELM_GENLIST_ITEM_NONE, _text_btn_cb, ad);
	}
	id = calloc(sizeof(item_data), 1);
	if (id) {
		id->index = index++;
		id->item = elm_genlist_item_append(genlist, itc, id, NULL, ELM_GENLIST_ITEM_NONE, _bottom_text_btn_cb, ad);
	}
	id = calloc(sizeof(item_data), 1);
	if (id) {
		id->index = index++;
		id->item = elm_genlist_item_append(genlist, itc, id, NULL, ELM_GENLIST_ITEM_NONE, _bottom_icon_btn_cb, ad);
	}

	elm_genlist_item_append(genlist, ptc, NULL, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);

	elm_genlist_item_class_free(ttc);
	elm_genlist_item_class_free(itc);
	elm_genlist_item_class_free(ptc);

	nf_it = elm_naviframe_item_push(ad->nf, "Button", NULL, NULL, genlist, "empty");
	elm_object_item_data_set(nf_it, circle_genlist);
}
