/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

#include "main.h"

Evas_Object *focused_obj = NULL;

static char *menu_its[] = {
	"Set timer",
	"Default",
	/* do not delete below */
	NULL
};

typedef struct _item_data {
	int index;
	Elm_Object_Item *item;
} item_data;

Evas_Object *second;

static void
gl_selected_cb(void *data, Evas_Object *obj, void *event_info)
{
	Elm_Object_Item *it = (Elm_Object_Item *)event_info;
	elm_genlist_item_selected_set(it, EINA_FALSE);
}

static char *
_gl_menu_title_text_get(void *data, Evas_Object *obj, const char *part)
{
	char buf[1024];

	snprintf(buf, 1023, "%s", _("Spinner"));
	return strdup(buf);
}

static char *
_gl_menu_text_get(void *data, Evas_Object *obj, const char *part)
{
	char buf[1024];
	item_data *id = (item_data *)data;
	int index = id->index;

	if (!strcmp(part, "elm.text")) {
		snprintf(buf, 1023, "%s", menu_its[index]);
		return strdup(buf);
	}
	return NULL;
}

static void
_gl_menu_del(void *data, Evas_Object *obj)
{
	// FIXME: Unrealized callback can be called after this.
	// Accessing Item_Data can be dangerous on unrealized callback.
	item_data *id = (item_data *)data;
	if (id) free(id);
}

static void _focused_cb(void *data, Evas_Object *obj, void *event_info)
{
	focused_obj = obj;
	eext_rotary_object_event_activated_set(obj, EINA_TRUE);
}

	static Eina_Bool
_spinner_naviframe_pop_cb(void *data, Elm_Object_Item *it)
{
	eext_rotary_object_event_activated_set(focused_obj, EINA_FALSE);

	return EINA_TRUE;
}

	static void
_btn_clicked(void *data, Evas_Object *obj, void *event_info)
{
	Evas_Object *nf = data;

	elm_naviframe_item_pop(nf);
}

static void _timer_cb(void *data, Evas_Object * obj, void *event_info)
{
	appdata_s *ad = data;
	Evas_Object *layout, *hour, *minute, *bottom_button;
	Elm_Object_Item *nf_it;

	layout = elm_layout_add(ad->nf);
	//This customized layout for two or more spinner object in one layout.
	elm_layout_file_set(layout, ELM_DEMO_EDJ, "spinner_layout");
	evas_object_show(layout);

	hour = elm_spinner_add(layout);
	elm_object_style_set(hour, "circle");
	//Circle spinner add for circular visual interaction with rotary event.
	eext_circle_object_spinner_add(hour, ad->circle_surface);
	elm_spinner_min_max_set(hour, 0.0, 23.0);
	elm_object_part_text_set(hour, "elm.text", "Hour");
	evas_object_smart_callback_add(hour, "focused", _focused_cb, hour);
	elm_object_part_content_set(layout, "hour", hour);

	minute = elm_spinner_add(layout);
	elm_object_style_set(minute, "circle");
	//Circle spinner add for circular visual interaction with rotary event.
	eext_circle_object_spinner_add(minute, ad->circle_surface);
	elm_spinner_min_max_set(minute, 0.0, 59.0);
	elm_object_part_text_set(minute, "elm.text", "Minute");
	evas_object_smart_callback_add(minute, "focused", _focused_cb, minute);
	elm_object_part_content_set(layout, "minute", minute);

	second = elm_spinner_add(layout);
	elm_object_style_set(second, "circle");
	//Circle spinner add for circular visual interaction with rotary event.
	eext_circle_object_spinner_add(second, ad->circle_surface);
	elm_spinner_min_max_set(second, 0.0, 59.0);
	elm_object_part_text_set(second, "elm.text", "Second");
	evas_object_smart_callback_add(second, "focused", _focused_cb, second);
	elm_object_part_content_set(layout, "second", second);

	bottom_button = elm_button_add(layout);
	elm_object_style_set(bottom_button, "bottom");
	elm_object_text_set(bottom_button, "SET");
	evas_object_smart_callback_add(bottom_button, "clicked", _btn_clicked, ad->nf);
	elm_object_part_content_set(layout, "elm.swallow.button", bottom_button);

	elm_object_part_text_set(layout, "elm.text.title", "Set timer");

	elm_object_focus_set(second, EINA_TRUE);

	nf_it = elm_naviframe_item_push(ad->nf, _("Spinner"), NULL, NULL, layout, NULL);
	elm_naviframe_item_title_enabled_set(nf_it, EINA_FALSE, EINA_FALSE);
	elm_naviframe_item_pop_cb_set(nf_it, _spinner_naviframe_pop_cb, NULL);
}

static void _default_cb(void *data, Evas_Object * obj, void *event_info)
{
	appdata_s *ad = data;
	Evas_Object *layout, *unit, *bottom_button, *circle_spinner;
	Elm_Object_Item *nf_it;

	layout = elm_layout_add(ad->nf);
	elm_layout_theme_set(layout, "layout", "circle", "spinner");

	evas_object_show(layout);

	unit = elm_spinner_add(layout);
	elm_object_style_set(unit, "circle");
	//Circle spinner add for circular visual interaction with rotary event.
	circle_spinner = eext_circle_object_spinner_add(unit, ad->circle_surface);
	//This angle per each spinner value will be multiplied with step value
	//and set the circle spinner picker offset, when every rotary event called.
	//If user not sets angle value, the angle value calc by below formula.
	//(360/ (Max - Min)).
	eext_circle_object_spinner_angle_set(circle_spinner, 2.0);
	evas_object_show(unit);

	elm_spinner_wrap_set(unit, EINA_FALSE);
	elm_spinner_label_format_set(unit, "%1.1f");
	elm_spinner_min_max_set(unit, 9.0, 99.0);
	elm_spinner_step_set(unit, 7.5);
	elm_object_part_text_set(unit, "elm.text", "unit");
	evas_object_smart_callback_add(unit, "focused", _focused_cb, unit);
	elm_object_part_content_set(layout, "elm.swallow.content", unit);

	bottom_button = elm_button_add(layout);
	elm_object_style_set(bottom_button, "bottom");
	elm_object_text_set(bottom_button, "SET");
	evas_object_smart_callback_add(bottom_button, "clicked", _btn_clicked, ad->nf);
	elm_object_part_content_set(layout, "elm.swallow.btn", bottom_button);

	elm_object_part_text_set(layout, "elm.text", "Title");
	nf_it = elm_naviframe_item_push(ad->nf, _("Spinner"), NULL, NULL, layout, NULL);
	elm_naviframe_item_title_enabled_set(nf_it, EINA_FALSE, EINA_FALSE);
	elm_naviframe_item_pop_cb_set(nf_it, _spinner_naviframe_pop_cb, NULL);
}

void spinner_cb(void *data, Evas_Object * obj, void *event_info)
{
	appdata_s *ad = (appdata_s *)data;
	Evas_Object *genlist;
	Evas_Object *circle_genlist;
	Elm_Genlist_Item_Class *itc = elm_genlist_item_class_new();
	Elm_Genlist_Item_Class *ttc = elm_genlist_item_class_new();
	Elm_Genlist_Item_Class *ptc = elm_genlist_item_class_new();
	Elm_Object_Item *nf_it;
	item_data *id;
	int index = 0;

	if (!ad) return;

	genlist = elm_genlist_add(ad->nf);
	elm_genlist_mode_set(genlist, ELM_LIST_COMPRESS);
	evas_object_smart_callback_add(genlist, "selected", gl_selected_cb, NULL);

	circle_genlist = eext_circle_object_genlist_add(genlist, ad->circle_surface);
	eext_circle_object_genlist_scroller_policy_set(circle_genlist, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
	eext_rotary_object_event_activated_set(circle_genlist, EINA_TRUE);

	ttc->item_style = "title";
	ttc->func.text_get = _gl_menu_title_text_get;
	ttc->func.del = _gl_menu_del;

	itc->item_style = "default";
	itc->func.text_get = _gl_menu_text_get;
	itc->func.del = _gl_menu_del;

	ptc->item_style = "padding";
	ptc->func.del = _gl_menu_del;

	elm_genlist_item_append(genlist, ttc, NULL, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);

	id = calloc(sizeof(item_data), 1);
	id->index = index++;
	id->item = elm_genlist_item_append(genlist, itc, id, NULL, ELM_GENLIST_ITEM_NONE, _timer_cb, ad);
	id = calloc(sizeof(item_data), 1);
	id->index = index++;
	id->item = elm_genlist_item_append(genlist, itc, id, NULL, ELM_GENLIST_ITEM_NONE, _default_cb, ad);

	Elm_Object_Item *padding_item;
	padding_item = elm_genlist_item_append(genlist, ptc, NULL, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);
	elm_atspi_accessible_role_set(padding_item, ELM_ATSPI_ROLE_REDUNDANT_OBJECT);

	elm_genlist_item_class_free(ttc);
	elm_genlist_item_class_free(itc);
	elm_genlist_item_class_free(ptc);

	nf_it = elm_naviframe_item_push(ad->nf, _("Spinner"), NULL, NULL, genlist, "empty");
	elm_object_item_data_set(nf_it, circle_genlist);
}
