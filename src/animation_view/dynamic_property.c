/*
 * Copyright (c) 2019 Samsung Electronics Co., Ltd All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
#include "main.h"


static Evas_Object*
create_dynamic_property_view(Evas_Object *parent)
{
   Evas_Object *box = elm_box_add(parent);
   evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_align_set(box, EVAS_HINT_FILL, EVAS_HINT_FILL);

   Evas_Object *label = elm_label_add(box);
   evas_object_size_hint_weight_set(label, EVAS_HINT_EXPAND, 0);
   evas_object_size_hint_align_set(label, 0.5, 0);
   elm_object_text_set(label, "Original");
   evas_object_show(label);
   elm_box_pack_end(box, label);

   Evas_Object *anim_view = elm_animation_view_add(box);
   evas_object_size_hint_weight_set(anim_view, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_align_set(anim_view, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_animation_view_file_set(anim_view, ICON_DIR"/done.json", NULL);
   evas_object_show(anim_view);
   elm_animation_view_auto_repeat_set(anim_view, EINA_TRUE);
   elm_animation_view_play(anim_view);
   evas_object_size_hint_min_set(anim_view, ELM_SCALE_SIZE(150), ELM_SCALE_SIZE(150));
   elm_box_pack_end(box, anim_view);

   label = elm_label_add(box);
   evas_object_size_hint_weight_set(label, EVAS_HINT_EXPAND, 0);
   evas_object_size_hint_align_set(label, 0.5, 0);
   elm_object_text_set(label, "Fill color property change");
   evas_object_show(label);
   elm_box_pack_end(box, label);

   Evas_Object *anim_view2 = elm_animation_view_add(box);
   evas_object_size_hint_weight_set(anim_view2, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_align_set(anim_view2, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_animation_view_file_set(anim_view2, ICON_DIR"/done.json", NULL);
   elm_animation_view_auto_repeat_set(anim_view2, EINA_TRUE);
   elm_animation_view_progress_set(anim_view2, 30);
   elm_animation_view_play(anim_view2);
   evas_object_size_hint_min_set(anim_view2, ELM_SCALE_SIZE(150), ELM_SCALE_SIZE(150));
   evas_object_show(anim_view2);
   elm_box_pack_end(box, anim_view2);

   Efl_Gfx_Vg_Value_Provider *vp = efl_add(EFL_GFX_VG_VALUE_PROVIDER_CLASS, box);
   efl_gfx_vg_value_provider_keypath_set(vp, "Shape Layer 1.Ellipse 1.Fill 1");
   efl_gfx_vg_value_provider_fill_color_set(vp, 255, 0 ,0 ,255);
   efl_ui_vg_animation_value_provider_override(anim_view2, vp);

   label = elm_label_add(box);
   evas_object_size_hint_weight_set(label, EVAS_HINT_EXPAND, 0);
   evas_object_size_hint_align_set(label, 0.5, 0);
   elm_object_text_set(label, "Stroke property change");
   evas_object_show(label);
   elm_box_pack_end(box, label);

   Evas_Object *anim_view3 = elm_animation_view_add(box);
   evas_object_size_hint_weight_set(anim_view3, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_align_set(anim_view3, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_animation_view_file_set(anim_view3, ICON_DIR"/done.json", NULL);
   elm_animation_view_auto_repeat_set(anim_view3, EINA_TRUE);
   elm_animation_view_progress_set(anim_view3, 60);
   elm_animation_view_play(anim_view3);
   evas_object_size_hint_min_set(anim_view3, ELM_SCALE_SIZE(150), ELM_SCALE_SIZE(150));
   evas_object_show(anim_view3);
   elm_box_pack_end(box, anim_view3);

   Efl_Gfx_Vg_Value_Provider *vp2 = efl_add(EFL_GFX_VG_VALUE_PROVIDER_CLASS, box);
   efl_gfx_vg_value_provider_keypath_set(vp2, "**");
   efl_gfx_vg_value_provider_stroke_color_set(vp2, 0, 0 ,255 ,100);
   efl_gfx_vg_value_provider_stroke_width_set(vp2, 8.0);
   efl_ui_vg_animation_value_provider_override(anim_view3, vp2);

   return box;
}

static Evas_Object*
create_scroller(Evas_Object *parent)
{
   Evas_Object *scroller = elm_scroller_add(parent);
   evas_object_size_hint_weight_set(scroller, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_align_set(scroller, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_scroller_bounce_set(scroller, EINA_TRUE, EINA_FALSE);
   elm_scroller_content_min_limit(scroller, 1, 0);
   evas_object_show(scroller);

   return scroller;
}

void
dynamic_property_cb(void *data, Evas_Object *obj, void *event_info)
{
   appdata_s *ad = (appdata_s *)data;
   Evas_Object *scroller, *circle_scroller, *content;
   Evas_Object *nf = ad->nf;
   Elm_Object_Item *nf_it;

   scroller = create_scroller(nf);
   content = create_dynamic_property_view(scroller);
   elm_object_content_set(scroller, content);

   circle_scroller = eext_circle_object_scroller_add(scroller, ad->circle_surface);
   eext_circle_object_scroller_policy_set(circle_scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
   eext_rotary_object_event_activated_set(circle_scroller, EINA_TRUE);

   nf_it = elm_naviframe_item_push(nf, "Dynamic property", NULL, NULL, scroller, NULL);
   elm_naviframe_item_title_enabled_set(nf_it, EINA_TRUE, EINA_FALSE);
}
