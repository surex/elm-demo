/*
 * Copyright (c) 2019 Samsung Electronics Co., Ltd All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
#include "main.h"


static void
btn_clicked_cb(void *data, Evas_Object *obj, void *event_info)
{
	Evas_Object *anim_view = data;
	const char *text = elm_object_text_get(obj);

	if (!text) return;

	if (!strcmp("Play", text))
		elm_animation_view_play(anim_view);
	else if (!strcmp("Pause", text))
		elm_animation_view_pause(anim_view);
	else if (!strcmp("Resume", text))
		elm_animation_view_resume(anim_view);
	else if (!strcmp("Play Back", text))
		elm_animation_view_play_back(anim_view);
	else if (!strcmp("Stop", text))
		elm_animation_view_stop(anim_view);
}

static void
update_anim_view_state(Evas_Object *anim_view, Evas_Object *label)
{
	Elm_Animation_View_State state = elm_animation_view_state_get(anim_view);

	switch (state)
	{
		case ELM_ANIMATION_VIEW_STATE_NOT_READY:
			elm_object_text_set(label, "State = Not Ready");
			break;
		case ELM_ANIMATION_VIEW_STATE_PLAY:
			elm_object_text_set(label, "State = Playing");
			break;
		case ELM_ANIMATION_VIEW_STATE_PLAY_BACK:
			elm_object_text_set(label, "State = Playing Back");
			break;
		case ELM_ANIMATION_VIEW_STATE_PAUSE:
			elm_object_text_set(label, "State = Paused");
			break;
		case ELM_ANIMATION_VIEW_STATE_STOP:
			elm_object_text_set(label, "State = Stopped");
			break;
	}
}

static void
_play_updated(void *data, Evas_Object *obj, void *ev)
{
	Evas_Object *slider = data;
	elm_slider_value_set(slider, elm_animation_view_progress_get(obj));
}

static void
_state_update(void *data, Evas_Object *obj, void *ev)
{
	Evas_Object *label = data;
	update_anim_view_state(obj, label);
}

static void
_slider_drag_cb(void *data, Evas_Object *obj, void *ev)
{
	Evas_Object *anim_view = data;
	elm_animation_view_progress_set(anim_view, elm_slider_value_get(obj));
}

static void
_slider_reset(void *data, Evas_Object *obj, void *ev)
{
	Evas_Object *slider = data;
	elm_slider_value_set(slider, 0);
}

static Evas_Object*
create_play_control_view(Evas_Object *parent)
{
	Evas_Object *box = elm_box_add(parent);
	elm_box_padding_set(box, ELM_SCALE_SIZE(10), ELM_SCALE_SIZE(10));
	evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(box, EVAS_HINT_FILL, EVAS_HINT_FILL);

	//State Text
	Evas_Object *label = elm_label_add(box);
	evas_object_size_hint_weight_set(label, EVAS_HINT_EXPAND, 0);
	evas_object_size_hint_align_set(label, 0.5, 0);
	evas_object_show(label);
	elm_box_pack_end(box, label);

	//Animation View
	Evas_Object *anim_view = elm_animation_view_add(box);
	evas_object_size_hint_weight_set(anim_view, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(anim_view, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_animation_view_file_set(anim_view, ICON_DIR"/running.json", NULL);
	evas_object_size_hint_min_set(anim_view, ELM_SCALE_SIZE(250), ELM_SCALE_SIZE(250));
	elm_animation_view_play(anim_view);
	elm_animation_view_auto_repeat_set(anim_view, EINA_TRUE);
	evas_object_show(anim_view);

	elm_box_pack_end(box, anim_view);

	//Controller Set: 0
	Evas_Object *box2 = elm_box_add(box);
	evas_object_size_hint_weight_set(box2, EVAS_HINT_EXPAND, 0);
	evas_object_size_hint_align_set(box2, EVAS_HINT_FILL, 1);
	elm_box_horizontal_set(box2, EINA_TRUE);
	elm_box_pack_end(box, box2);
	evas_object_show(box2);

	//Slider
	Evas_Object *slider = elm_slider_add(box);
	elm_slider_min_max_set(slider, 0, 1);
	evas_object_size_hint_weight_set(slider, EVAS_HINT_EXPAND, 0);
	evas_object_size_hint_align_set(slider, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_smart_callback_add(slider, "changed", _slider_drag_cb, anim_view);
	evas_object_show(slider);
	elm_box_pack_end(box, slider);

	//Controller Set: 1
	Evas_Object *box3 = elm_box_add(box);
	evas_object_size_hint_weight_set(box3, EVAS_HINT_EXPAND, 0);
	evas_object_size_hint_align_set(box3, EVAS_HINT_FILL, 1);
	elm_box_horizontal_set(box3, EINA_TRUE);
	elm_box_pack_end(box, box3);
	evas_object_show(box3);

	Evas_Object *btn;

	//Play Button
	btn = elm_button_add(box3);
	evas_object_size_hint_weight_set(btn, EVAS_HINT_EXPAND, 0);
	evas_object_size_hint_align_set(btn, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_object_text_set(btn, "Play");
	evas_object_show(btn);
	evas_object_smart_callback_add(btn, "clicked", btn_clicked_cb, anim_view);
	elm_box_pack_end(box3, btn);

	//Play Back Button
	btn = elm_button_add(box3);
	evas_object_size_hint_weight_set(btn, EVAS_HINT_EXPAND, 0);
	evas_object_size_hint_align_set(btn, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_object_text_set(btn, "Play Back");
	evas_object_smart_callback_add(btn, "clicked", btn_clicked_cb, anim_view);
	evas_object_show(btn);
	elm_box_pack_end(box3, btn);

	//Stop Button
	btn = elm_button_add(box3);
	evas_object_size_hint_weight_set(btn, EVAS_HINT_EXPAND, 0);
	evas_object_size_hint_align_set(btn, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_object_text_set(btn, "Stop");
	evas_object_smart_callback_add(btn, "clicked", btn_clicked_cb, anim_view);
	evas_object_show(btn);
	elm_box_pack_end(box3, btn);

	//Controller Set: 2
	Evas_Object *box4 = elm_box_add(box);
	evas_object_size_hint_weight_set(box4, EVAS_HINT_EXPAND, 0);
	evas_object_size_hint_align_set(box4, EVAS_HINT_FILL, 1);
	elm_box_horizontal_set(box4, EINA_TRUE);
	elm_box_pack_end(box, box4);
	evas_object_show(box4);

	//Pause Button
	btn = elm_button_add(box4);
	elm_object_text_set(btn, "Pause");
	evas_object_size_hint_weight_set(btn, EVAS_HINT_EXPAND, 0);
	evas_object_size_hint_align_set(btn, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_smart_callback_add(btn, "clicked", btn_clicked_cb, anim_view);
	evas_object_show(btn);
	elm_box_pack_end(box4, btn);

	//Resume Button
	btn = elm_button_add(box4);
	evas_object_size_hint_weight_set(btn, EVAS_HINT_EXPAND, 0);
	evas_object_size_hint_align_set(btn, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_object_text_set(btn, "Resume");
	evas_object_smart_callback_add(btn, "clicked", btn_clicked_cb, anim_view);
	evas_object_show(btn);
	elm_box_pack_end(box4, btn);

	//Empty Space
	Evas_Object *box5 = elm_box_add(box);
	evas_object_size_hint_weight_set(box5, EVAS_HINT_EXPAND, 0);
	evas_object_size_hint_align_set(box5, EVAS_HINT_FILL, 1);
	elm_box_horizontal_set(box5, EINA_TRUE);
	elm_box_pack_end(box, box5);
	evas_object_show(box5);

	Evas_Object *rect = evas_object_rectangle_add(evas_object_evas_get(box5));
	evas_object_size_hint_weight_set(rect, EVAS_HINT_EXPAND, 0);
	evas_object_size_hint_align_set(rect, EVAS_HINT_FILL, EVAS_HINT_FILL);
        evas_object_size_hint_min_set(rect, 150, 70);
        evas_object_color_set(rect, 0, 0 ,0 ,0);
	evas_object_show(rect);
	elm_box_pack_end(box5, rect);

	evas_object_smart_callback_add(anim_view, "play,start", _state_update, label);
	evas_object_smart_callback_add(anim_view, "play,stop", _state_update, label);
	evas_object_smart_callback_add(anim_view, "play,pause", _state_update, label);
	evas_object_smart_callback_add(anim_view, "play,resume", _state_update, label);

	evas_object_smart_callback_add(anim_view, "play,update", _play_updated, slider);
	evas_object_smart_callback_add(anim_view, "play,stop", _slider_reset, slider);

	update_anim_view_state(anim_view, label);

	return box;
}

static Evas_Object*
create_scroller(Evas_Object *parent)
{
	Evas_Object *scroller = elm_scroller_add(parent);
	evas_object_size_hint_weight_set(scroller, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(scroller, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_scroller_bounce_set(scroller, EINA_TRUE, EINA_FALSE);
	elm_scroller_content_min_limit(scroller, 1, 0);
	evas_object_show(scroller);

	return scroller;
}

void
play_control_cb(void *data, Evas_Object *obj, void *event_info)
{
	appdata_s *ad = (appdata_s *)data;
	Evas_Object *scroller, *circle_scroller, *layout;
	Evas_Object *nf = ad->nf;
	Elm_Object_Item *nf_it;

	scroller = create_scroller(nf);
	layout = create_play_control_view(scroller);
	elm_object_content_set(scroller, layout);

	circle_scroller = eext_circle_object_scroller_add(scroller, ad->circle_surface);
	eext_circle_object_scroller_policy_set(circle_scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
	eext_rotary_object_event_activated_set(circle_scroller, EINA_TRUE);

	nf_it = elm_naviframe_item_push(nf, "Play Control", NULL, NULL, scroller, NULL);
	elm_naviframe_item_title_enabled_set(nf_it, EINA_TRUE, EINA_FALSE);
}
