/*
 * Copyright (c) 2019 Samsung Electronics Co., Ltd All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
#include "main.h"

static Evas_Object*
create_watchface_view(Evas_Object *parent)
{
	Evas_Object *btn, *box;

	box = elm_box_add(parent);
	evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(box, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_box_padding_set(box, 0, ELM_SCALE_SIZE(25));
	evas_object_show(box);

	//Animation View
	Evas_Object *anim_view = elm_animation_view_add(box);
	evas_object_size_hint_weight_set(anim_view, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(anim_view, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_animation_view_file_set(anim_view, ICON_DIR"/watchface.json", NULL);
	elm_animation_view_play(anim_view);
	elm_animation_view_auto_repeat_set(anim_view, EINA_TRUE);
	evas_object_show(anim_view);

	elm_box_pack_end(box, anim_view);

	return box;
}

void
watchface_cb(void *data, Evas_Object *obj, void *event_info)
{
	appdata_s *ad = (appdata_s *)data;
	Evas_Object *content;
	Evas_Object *nf = ad->nf;
	Elm_Object_Item *nf_it;

	content = create_watchface_view(nf);

	nf_it = elm_naviframe_item_push(nf, "Default Styles", NULL, NULL, content, NULL);
	elm_naviframe_item_title_enabled_set(nf_it, EINA_FALSE, EINA_FALSE);
}
