/*
 * Copyright (c) 2019 Samsung Electronics Co., Ltd All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *	 http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
#include "main.h"

#define IMAGE_MAX 5
#define BUFFER_SIZE_MAX 1024

typedef struct
{
	Evas_Object *gengrid;
	appdata_s *appdata;
	int item_size;
} _view_data_s;

typedef struct item_data
{
	Elm_Object_Item *item;
	int index;
	const char *path;
	_view_data_s *view_data;
} _item_data_s;

static const char *menu_items[] = {
	"horizontal - 3 Row",
	"horizontal - 2 Row",
	"vertical - 1 Column",
	"vertical - 2 Column",
	//"type2",
	/* do not delete below */
	NULL
};

static const char *gengrid_demo_names[] = {
	"AAAAA",
	"BBBBB",
	"CCCCC",
	"DDDDD",
	"EEEEE",
	NULL
};

static Evas_Object*
__gengrid_content_get_cb(void *data, Evas_Object *obj, const char *part)
{
	_item_data_s *id = data;
	Evas_Object* content = NULL;

	if (!strcmp(part, "elm.swallow.icon"))
	{
		content = elm_image_add(obj);

		elm_image_file_set(content, id->path, NULL);
		elm_image_aspect_fixed_set(content, EINA_FALSE);
		elm_image_preload_disabled_set(content, EINA_FALSE);
		evas_object_show(content);
	}

	return content;
}

static char*
__gengrid_text_get_cb(void *data, Evas_Object *obj, const char *part)
{
	_item_data_s *id = data;
	size_t size = sizeof(gengrid_demo_names)/sizeof(char *) - 1;

	if (!strcmp(part, "elm.text"))
	{
		return strdup(gengrid_demo_names[id->index % size]);
	}

	return NULL;
}

static void
__gengrid_realized_cb(void *data, Evas_Object *obj, void *event_info)
{
	printf("[%s][%d]\n", __func__, __LINE__);
}

static void
__gengrid_item_selected_cb(void *data, Evas_Object *obj, void *event_info)
{
	Elm_Object_Item *it = event_info;

	printf("item selected: %p\n", it);
	elm_gengrid_item_selected_set(it, EINA_FALSE);
}

static char *
__gengrid_menu_item_text_get_cb(void *data, Evas_Object *obj, const char *part)
{
	char buf[BUFFER_SIZE_MAX] = "";
	_item_data_s *item_data = data;
	int index = item_data->index;

	if (!strcmp(part, "elm.text"))
	{
		snprintf(buf, sizeof(buf), "%s", menu_items[index]);

		return strdup(buf);
	}

	return NULL;
}

static void
__gengrid_item_delete_cb(void *data, Evas_Object *obj)
{
	_item_data_s *item_data = (_item_data_s *) data;
	if (item_data) free(item_data);
}

static void
__genlist_item_delete_cb(void *data, Evas_Object *obj)
{
	_item_data_s *item_data = (_item_data_s *) data;
	if (item_data) free(item_data);
}

static Eina_Bool
__naviframe_item_pop_cb(void *data, Elm_Object_Item *item)
{
	Evas_Object *circle_object = data;

	if (!circle_object) return EINA_FALSE;

	eext_rotary_object_event_activated_set(circle_object, EINA_FALSE);

	return EINA_TRUE;
}

static void
__gengrid_menu_item_selected_cb(void *data, Evas_Object *obj, void *event_info)
{
	if (!data || !event_info) return;

	_item_data_s *item_data = data;
	const char *style = menu_items[item_data->index];

	elm_genlist_item_selected_set((Elm_Object_Item *)event_info, EINA_FALSE);
	_view_data_s *view_data = item_data->view_data;
	appdata_s *ad = view_data->appdata;
	Elm_Object_Item *naviframe_item;

	Elm_Gengrid_Item_Class *itc;
	char buf[PATH_MAX];
	int i = 0, j = 0, n = 0;

	if (!ad->nf) return;

	view_data->gengrid = elm_gengrid_add(ad->nf);
	elm_object_style_set(view_data->gengrid, "circle");
	if (strstr(style, "horizontal"))
	  elm_gengrid_horizontal_set(view_data->gengrid, EINA_TRUE);


	elm_object_scroll_item_align_enabled_set(view_data->gengrid, EINA_TRUE);

	Evas_Object *circle_gengrid = eext_circle_object_gengrid_add(view_data->gengrid, item_data->view_data->appdata->circle_surface);
	eext_circle_object_gengrid_scroller_policy_set(circle_gengrid, ELM_SCROLLER_POLICY_AUTO, ELM_SCROLLER_POLICY_AUTO);
	eext_rotary_object_event_activated_set(circle_gengrid, EINA_TRUE);

	evas_object_size_hint_weight_set(view_data->gengrid, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(view_data->gengrid, EVAS_HINT_FILL, EVAS_HINT_FILL);

	evas_object_smart_callback_add(view_data->gengrid, "realized", __gengrid_realized_cb, NULL);

	itc = elm_gengrid_item_class_new();

	if (!strcmp(style, "horizontal - 3 Row"))
	{
        elm_gengrid_item_size_set(view_data->gengrid, 109, 109);
		itc->item_style = "default";
	}
	else if (!strcmp(style, "horizontal - 2 Row"))
	{
        elm_gengrid_item_size_set(view_data->gengrid, 138, 138);
		itc->item_style = "default";
	}
	else if (!strcmp(style, "vertical - 1 Column"))
	{
        elm_gengrid_item_size_set(view_data->gengrid, 254, 188);
		itc->item_style = "vertical_single";
	}
	else if (!strcmp(style, "vertical - 2 Column"))
	{
	    elm_gengrid_item_size_set(view_data->gengrid, 129, 129);
		itc->item_style = "vertical";
	}
	itc->func.text_get = __gengrid_text_get_cb;
	itc->func.content_get = __gengrid_content_get_cb;
	itc->func.state_get = NULL;
	itc->func.del = __gengrid_item_delete_cb;

	for (j = 0; j < 100; ++j)
	{
		for (i = 0; i < IMAGE_MAX; ++i)
		{
			n = i + (j * IMAGE_MAX);
			snprintf(buf, sizeof(buf), ICON_DIR"/100_%d.jpg", (i+1));
			item_data = calloc(sizeof(_item_data_s), 1);
			item_data->index = n;
			item_data->path = eina_stringshare_add(buf);
			item_data->view_data = view_data;
			item_data->item = elm_gengrid_item_append(view_data->gengrid, itc, item_data, __gengrid_item_selected_cb, item_data);
			size_t size = sizeof(gengrid_demo_names)/sizeof(char *) - 1;
			elm_atspi_accessible_name_set(item_data->item, gengrid_demo_names[n % size]);
		}
	}
	elm_gengrid_item_class_free(itc);

	evas_object_show(view_data->gengrid);

	naviframe_item = elm_naviframe_item_push(ad->nf, NULL, NULL, NULL, view_data->gengrid, "empty");
	elm_naviframe_item_title_enabled_set(naviframe_item, EINA_FALSE, EINA_FALSE);
	elm_naviframe_item_pop_cb_set(naviframe_item, __naviframe_item_pop_cb, circle_gengrid);

}

static char *
__gengrid_menu_item_title_text_get_cb(void *data, Evas_Object *obj, const char *part)
{
	return strdup("Circle Gengrid");
}

static void
_genlist_del_cb(void *data, Evas *e, Evas_Object *obj, void *event_info)
{
	_view_data_s *view_data = (_view_data_s *)data;
	if (view_data) free(view_data);
}

void
gengrid_cb(void *data, Evas_Object *obj, void *event_info)
{
	appdata_s *ad = data;
	Evas_Object *genlist = NULL, *circle_genlist = NULL;
	Elm_Object_Item *naviframe_it = NULL;
	Elm_Object_Item *item = NULL;
	_item_data_s *item_data = NULL;
	_view_data_s *view_data = calloc(sizeof(_view_data_s), 1);
	int i = 0;

	if (!ad) return;

	genlist = elm_genlist_add(ad->nf);
	elm_genlist_mode_set(genlist, ELM_LIST_COMPRESS);
	elm_genlist_homogeneous_set(genlist, EINA_TRUE);

	circle_genlist = eext_circle_object_genlist_add(genlist, ad->circle_surface);
	eext_circle_object_genlist_scroller_policy_set(circle_genlist, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
	eext_rotary_object_event_activated_set(circle_genlist, EINA_TRUE);

	view_data->appdata = ad;

	Elm_Genlist_Item_Class *tic = elm_genlist_item_class_new();
	Elm_Genlist_Item_Class *itc = elm_genlist_item_class_new();
	Elm_Genlist_Item_Class *pic = elm_genlist_item_class_new();

	tic->item_style = "title";
	tic->func.text_get = __gengrid_menu_item_title_text_get_cb;

	pic->item_style = "padding";

	elm_genlist_item_append(
				genlist,
				tic,
				NULL,
				NULL,
				ELM_GENLIST_ITEM_NONE,
				NULL,
				NULL);

	itc->item_style = "1text";
	itc->func.text_get = __gengrid_menu_item_text_get_cb;
	itc->func.del = __genlist_item_delete_cb;

	for (i = 0; menu_items[i]; ++i)
	{
		item_data = calloc(sizeof(_item_data_s), 1);
		item_data->index = i;

		item = elm_genlist_item_append(
				genlist,
				itc,
				item_data,
				NULL,
				ELM_GENLIST_ITEM_NONE,
				__gengrid_menu_item_selected_cb,
				item_data);
		item_data->item = item;
		item_data->view_data = view_data;
	}

	elm_genlist_item_append(
			genlist,
			pic,
			NULL,
			NULL,
			ELM_GENLIST_ITEM_NONE,
			NULL,
			NULL);

	elm_genlist_item_class_free(tic);
	elm_genlist_item_class_free(itc);
	elm_genlist_item_class_free(pic);

	naviframe_it = elm_naviframe_item_push(ad->nf, "Gengrid", NULL, NULL, genlist, NULL);
	elm_naviframe_item_title_enabled_set(naviframe_it, EINA_FALSE, EINA_FALSE);
	evas_object_event_callback_add(genlist, EVAS_CALLBACK_DEL, _genlist_del_cb, (void *) view_data);
}
