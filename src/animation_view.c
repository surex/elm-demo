/*
 * Copyright (c) 2019 Samsung Electronics Co., Ltd All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

#include "main.h"

static void
gl_selected_cb(void *data, Evas_Object *obj, void *event_info)
{
	Elm_Object_Item *it = (Elm_Object_Item *)event_info;
	elm_genlist_item_selected_set(it, EINA_FALSE);
}

static char *
_gl_menu_title_text_get(void *data, Evas_Object *obj, const char *part)
{
	char buf[1024];
	snprintf(buf, 1023, "Animation View");
	return strdup(buf);
}

static char *
_gl_play_control_item_text_get(void *data, Evas_Object *obj, const char *part)
{
	char buf[1024];

	if (!strcmp(part, "elm.text")) {
		snprintf(buf, 1023, "Play Control");
		return strdup(buf);
	}
	return NULL;
}

static char *
_gl_dynamic_property_item_text_get(void *data, Evas_Object *obj, const char *part)
{
	char buf[1024];

	if (!strcmp(part, "elm.text")) {
		snprintf(buf, 1023, "Dynamic Property");
		return strdup(buf);
	}
	return NULL;
}

static char *
_gl_watchface_item_text_get(void *data, Evas_Object *obj, const char *part)
{
	char buf[1024];

	if (!strcmp(part, "elm.text")) {
		snprintf(buf, 1023, "Watchface");
		return strdup(buf);
	}
	return NULL;
}

void
animation_view_cb(void *data, Evas_Object *obj, void *event_info)
{
	appdata_s *ad = NULL;
	Evas_Object *genlist = NULL;
	Evas_Object *circle_genlist;
	Elm_Genlist_Item_Class *ttc = elm_genlist_item_class_new();

	Elm_Genlist_Item_Class *play_control_itc = elm_genlist_item_class_new();
	Elm_Genlist_Item_Class *watchface_itc = elm_genlist_item_class_new();
	Elm_Genlist_Item_Class *dynamic_property_itc = elm_genlist_item_class_new();

	Elm_Genlist_Item_Class *ptc = elm_genlist_item_class_new();

	ad = (appdata_s *) data;
	if (ad == NULL) return;

	genlist = elm_genlist_add(ad->nf);
	elm_genlist_mode_set(genlist, ELM_LIST_COMPRESS);
	evas_object_smart_callback_add(genlist, "selected", gl_selected_cb, NULL);

	circle_genlist = eext_circle_object_genlist_add(genlist, ad->circle_surface);
	eext_circle_object_genlist_scroller_policy_set(circle_genlist, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
	eext_rotary_object_event_activated_set(circle_genlist, EINA_TRUE);

	ttc->item_style = "title";
	ttc->func.text_get = _gl_menu_title_text_get;
	elm_genlist_item_append(genlist, ttc, NULL, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);

	play_control_itc->item_style = "default";
	play_control_itc->func.text_get = _gl_play_control_item_text_get;
	elm_genlist_item_append(genlist, play_control_itc, NULL, NULL, ELM_GENLIST_ITEM_NONE, play_control_cb, ad);

	watchface_itc->item_style = "default";
	watchface_itc->func.text_get = _gl_watchface_item_text_get;
	elm_genlist_item_append(genlist, watchface_itc, NULL, NULL, ELM_GENLIST_ITEM_NONE, watchface_cb, ad);

	dynamic_property_itc->item_style = "default";
	dynamic_property_itc->func.text_get = _gl_dynamic_property_item_text_get;
	elm_genlist_item_append(genlist, dynamic_property_itc, NULL, NULL, ELM_GENLIST_ITEM_NONE, dynamic_property_cb, ad);

	/* Genlist Padding Item style */
	ptc->item_style = "padding";
	elm_genlist_item_append(genlist, ptc, NULL, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);

	elm_genlist_item_class_free(ttc);
	elm_genlist_item_class_free(play_control_itc);
	elm_genlist_item_class_free(watchface_itc);
	elm_genlist_item_class_free(dynamic_property_itc);

	elm_naviframe_item_push(ad->nf, NULL, NULL, NULL, genlist, "empty");
}
