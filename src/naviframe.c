/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

#include "main.h"

typedef struct
{
	const char *name;
	void (*func)(void *data, Evas_Object *obj, void *event_info);
} menu_item_s;

static void _genlist_item_show_seamless_effect_selected_cb(void *data, Evas_Object *obj, void *event_info);

static const menu_item_s menu_items[] = {
	{"Seamless effect", _genlist_item_show_seamless_effect_selected_cb},
	{NULL, NULL}
};

static char*
_gl_text_get(void *data, Evas_Object *obj EINA_UNUSED, const char *part)
{
	if (!data) return NULL;
	return strdup(data);
}

static void
_genlist_item_clicked_cb(void *data, Evas_Object * obj, void *event_info)
{
	Elm_Object_Item *it = (Elm_Object_Item *) elm_list_selected_item_get(obj);

	if (!it)
	{
		LOGE("list item is NULL\n");
		return;
	}

	elm_list_item_selected_set(it, EINA_FALSE);
}

static Eina_Bool
_naviframe_item_pop_cb(void *data, Elm_Object_Item *item)
{
	Evas_Object *circle_object = data;

	if (!circle_object) return EINA_FALSE;

	eext_rotary_object_event_activated_set(circle_object, EINA_FALSE);

	return EINA_TRUE;
}

static void
_image_clicked_cb(void *data, Evas_Object *obj, void *event_info)
{
	Evas_Object *layout = NULL, *image = NULL;
	Elm_Object_Item *navi_it = NULL;

	appdata_s *ad = data;
	if (!ad) return;

	Evas_Object *naviframe = ad->nf;
	if (naviframe == NULL) return;

	layout = elm_layout_add(naviframe);
	elm_layout_theme_set(layout, "layout", "nocontents", "default");
	elm_object_part_text_set(layout, "elm.text", "Sample string");
	elm_object_part_text_set(layout, "elm.text.title", "Title");

	image = elm_image_add(layout);
	elm_image_file_set(image, ICON_DIR "/g1.png", NULL);
	elm_object_part_content_set(layout, "elm.swallow.icon", image);

	navi_it = elm_naviframe_item_push_from(naviframe, NULL, NULL, NULL, layout, NULL, obj);
	elm_naviframe_item_title_enabled_set(navi_it, EINA_FALSE, EINA_FALSE);
}

static void
_genlist_item_show_seamless_effect_selected_cb(void *data, Evas_Object *obj, void *event_info)
{
	elm_genlist_item_selected_set((Elm_Object_Item *)event_info, EINA_FALSE);

	Evas_Object *layout = NULL, *image = NULL;
	Elm_Object_Item *navi_it = NULL;

	appdata_s *ad = data;
	if (!ad) return;

	Evas_Object *naviframe = ad->nf;
	if (naviframe == NULL) return;

	layout = elm_layout_add(naviframe);
	elm_layout_theme_set(layout, "layout", "nocontents", "default");
	elm_object_part_text_set(layout, "elm.text", "Tap on image");
	elm_object_part_text_set(layout, "elm.text.title", "Show Seamless");

	image = elm_image_add(layout);
	elm_image_file_set(image, ICON_DIR "/g2.png", NULL);
	elm_object_part_content_set(layout, "elm.swallow.icon", image);
	evas_object_smart_callback_add(image, "clicked", _image_clicked_cb, ad);

	navi_it = elm_naviframe_item_push(naviframe, NULL, NULL, NULL, layout, NULL);
	elm_naviframe_item_title_enabled_set(navi_it, EINA_FALSE, EINA_FALSE);
}

static Evas_Object *
_create_genlist(appdata_s *data)
{
	const menu_item_s *menu_its = menu_items;
	int idx = 0;

	appdata_s *ad = data;
	if (!ad) return NULL;

	Evas_Object *naviframe = ad->nf;
	if (!naviframe) return NULL;

	Evas_Object *genlist = elm_genlist_add(naviframe);

	elm_genlist_homogeneous_set(genlist, EINA_TRUE);
	elm_genlist_mode_set(genlist, ELM_LIST_COMPRESS);

	evas_object_smart_callback_add(genlist, "selected", _genlist_item_clicked_cb, NULL);

	Elm_Genlist_Item_Class *title_item = elm_genlist_item_class_new();
	title_item->item_style = "title";
	title_item->func.text_get = _gl_text_get;

	elm_genlist_item_append(genlist, title_item, "Naviframe", NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);
	elm_genlist_item_class_free(title_item);

	Elm_Genlist_Item_Class *genlist_item = elm_genlist_item_class_new();
	genlist_item->item_style = "1text";
	genlist_item->func.text_get = _gl_text_get;

	while (menu_its[idx].name != NULL)
	{
		elm_genlist_item_append(genlist, genlist_item, menu_its[idx].name, NULL, ELM_GENLIST_ITEM_NONE, menu_its[idx].func, ad);
		 ++idx;
	}
	Elm_Genlist_Item_Class *ptc = elm_genlist_item_class_new();
	ptc->item_style = "padding";
	elm_genlist_item_append(genlist, ptc, NULL, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);

	elm_genlist_item_class_free(ptc);
	elm_genlist_item_class_free(genlist_item);

	return genlist;
}

void
naviframe_cb(void *data, Evas_Object *obj, void *event_info)
{
	appdata_s *app_data = data;
	if (!app_data) return;

	Evas_Object *naviframe = app_data->nf;
	if (!naviframe) return;
	Elm_Object_Item *it = NULL;

	Evas_Object *genlist = _create_genlist(app_data);

	Evas_Object *circle_genlist = eext_circle_object_genlist_add(genlist, app_data->circle_surface);
	eext_rotary_object_event_activated_set(circle_genlist, EINA_TRUE);

	it = elm_naviframe_item_push(naviframe, "Scroller", NULL, NULL, genlist, NULL);
	elm_naviframe_item_title_enabled_set(it, EINA_FALSE, EINA_FALSE);
	elm_naviframe_item_pop_cb_set(it, _naviframe_item_pop_cb, circle_genlist);
}
