#include "main.h"
#define DBL_EQ(a, b) (!!(fabs((double)a - (double)b) <= DBL_EPSILON))

#define N_ITEMS 2000
#define BUFFER_SIZE 256
#define TIME_SET 5.0

#define SCROLL_DISTANCE 999 * 10

int frame_cnt, ecore_cnt;
double entering_speed, frame_set, start_t;
Ecore_Animator *anim;
Elm_Object_Item *target_item = NULL;

typedef struct{
     appdata_s *ad;
     Evas_Object *obj;
     int start_y;
} perf_data;

static void _start_check_fps(Evas_Object *obj);
static void _stop_check_fps(void *data, Evas_Object *obj);

static Eina_Bool
_scroll_anim(void *data, double pos)
{
   perf_data *pd = data;
   int cur_x, cur_y, cur_w, cur_h;

   elm_scroller_region_get(pd->obj, &cur_x, &cur_y, &cur_w, &cur_h);

   cur_y = pd->start_y + ( SCROLL_DISTANCE * pos );

   elm_scroller_region_show(pd->obj, cur_x, cur_y, cur_w, cur_h);

   ecore_cnt++;

   if (DBL_EQ(pos, 1.0)) {
        _stop_check_fps(pd->ad, pd->obj);
        ecore_animator_del(anim);
        anim = NULL;
        return EINA_FALSE;
   }

   return EINA_TRUE;
}

static void
_render_post_cb(void *data,
                Evas *e EINA_UNUSED,
                void *event_info)
{
   frame_cnt++;
}

static void
_check_entering_speed(void *data,
                      Evas *e,
                      void *event_info)
{
   perf_data *pd = data;
   evas_event_callback_del(e, EVAS_CALLBACK_RENDER_POST, _check_entering_speed);
   entering_speed = (ecore_time_get() - entering_speed) * 1000;
   if (!anim)
   anim = ecore_animator_timeline_add(TIME_SET, _scroll_anim, pd);

   elm_scroller_region_get(pd->obj, NULL, &pd->start_y, NULL, NULL);
   _start_check_fps(pd->obj);

}

static void
_start_calc(void *data, Evas_Object *obj, void *event_info EINA_UNUSED)
{
   entering_speed = ecore_time_get();
   evas_object_smart_callback_del(obj, "loaded", _start_calc);
   evas_event_callback_add(evas_object_evas_get(obj), EVAS_CALLBACK_RENDER_POST, _check_entering_speed, data);
}

static void
_start_check_fps(Evas_Object *obj)
{
   Evas *e = evas_object_evas_get(obj);
   ecore_cnt = 0;
   start_t = ecore_time_get();
   evas_event_callback_add(e, EVAS_CALLBACK_RENDER_POST, _render_post_cb, NULL);
}

static void
_popup_hide_cb(void *data, Evas_Object *obj, void *event_info)
{
   if (!obj) return;
   elm_popup_dismiss(obj);
}

static void
_popup_hide_finished_cb(void *data, Evas_Object *obj, void *event_info)
{
   if (!obj) return;
   evas_object_del(obj);
}

static void
_stop_check_fps(void *data, Evas_Object *obj)
{
   Evas_Object *popup;
   Evas_Object *layout;
   appdata_s *ad = (appdata_s *)data;
   double end_t = ecore_time_get();
   end_t = end_t - start_t;

   double fps = 0.0, animator_fps = 0.0;
   char buf[BUFFER_SIZE];
   Evas *e = evas_object_evas_get(obj);

   evas_event_callback_del(e, EVAS_CALLBACK_RENDER_POST, _render_post_cb);
   ecore_animator_del(anim);

   fps = frame_cnt / end_t;
   animator_fps = ecore_cnt / end_t;

   dlog_print(DLOG_DEBUG, LOG_TAG, "Entering Speed : %3.1f msec", entering_speed);
   dlog_print(DLOG_DEBUG, LOG_TAG, "Animator FPS : %3.1f fps", animator_fps);
   dlog_print(DLOG_DEBUG, LOG_TAG, "Evas FPS : %3.1f fps", fps);

   snprintf(buf, sizeof(buf),
           "<font_size=27>"
           "Entering Speed : %3.1f msec<br>"
           "Animator FPS : %3.1f fps<br>"
           "Evas FPS : %3.1f fps"
           "</font_size>",
           entering_speed, animator_fps, fps);

   popup = elm_popup_add(ad->win);
   elm_object_style_set(popup, "circle");
   evas_object_size_hint_weight_set(popup, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   eext_object_event_callback_add(popup, EEXT_CALLBACK_BACK, _popup_hide_cb, NULL);
   evas_object_smart_callback_add(popup, "dismissed", _popup_hide_finished_cb, NULL);

   layout = elm_layout_add(popup);
   elm_layout_theme_set(layout, "layout", "popup", "content/circle");

   elm_object_part_text_set(layout, "elm.text", buf);
   elm_object_content_set(popup, layout);

   evas_object_show(popup);
}

static char *
_item_label_get(void *data, Evas_Object *obj, const char *part)
{
   char buf[BUFFER_SIZE];
   int count = (int) data % 10;
   if (!strcmp(part, "elm.text"))
     {
        if (count == 0) sprintf(buf, "%s", "Time Warner Cable(Cable)");
        else if ((int) count == 1) sprintf(buf, "%s", "ComCast (Cable)");
        else if ((int) count == 2) sprintf(buf, "%s", "Dish (Satellite)");
        else if ((int) count == 3) sprintf(buf, "%s", "DirecTV (Satellite)");
        else if ((int) count == 4) sprintf(buf, "%s", "Tata Sky (Satellite)");
        else if ((int) count == 5) sprintf(buf, "%s", "Nextra Cable(Cable)");
        else if ((int) count == 6) sprintf(buf, "%s", "DD Plus (Cable)");
        else if ((int) count == 7) sprintf(buf, "%s", "Tikona Cable(Cable)");
        else if ((int) count == 8) sprintf(buf, "%s", "True Provider (Cable)");
        else if ((int) count == 9) sprintf(buf, "%s", "Vodafone (Satellite)");
        else sprintf(buf, "%s", "Sample Text");
     }

   return strdup(buf);
}

static char *
_gl_menu_title_text_get(void *data, Evas_Object *obj, const char *part)
{
   char buf[1024];

   snprintf(buf, 1023, "%s", "Performance Test");
   return strdup(buf);
}

static Eina_Bool
_nf_pop_cb(void *data, Elm_Object_Item *it)
{
   free(data);

   return EINA_TRUE;
}

void performance_cb(void *data, Evas_Object *obj, void *event_info)
{
   appdata_s *ad = NULL;
   Evas_Object *gl = NULL;
   Evas_Object *circle_genlist;
   Elm_Genlist_Item_Class *itc = elm_genlist_item_class_new();
   Elm_Genlist_Item_Class *ttc = elm_genlist_item_class_new();
   Elm_Genlist_Item_Class *ptc = elm_genlist_item_class_new();
   int i = 0;

   ad = (appdata_s *)data;
   if (ad == NULL) return;

   perf_data *pd = calloc(1,sizeof(perf_data));
   pd->ad = ad;

   frame_set = 0.0;
   frame_cnt = 0;
   entering_speed = 0.0;

   pd->obj = gl = elm_genlist_add(ad->nf);
   elm_genlist_mode_set(gl, ELM_LIST_COMPRESS);
   elm_genlist_homogeneous_set(gl, EINA_TRUE);

   circle_genlist = eext_circle_object_genlist_add(gl, ad->circle_surface);
   eext_circle_object_genlist_scroller_policy_set(circle_genlist, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
   //eext_rotary_object_event_activated_set(circle_genlist, EINA_TRUE);

   ttc->item_style = "title";
   ttc->func.text_get = _gl_menu_title_text_get;
   ttc->func.del = NULL;

   itc->item_style = "default";
   itc->func.text_get = _item_label_get;
   itc->func.del = NULL;

   ptc->item_style = "padding";
   ptc->func.del = NULL;

   elm_genlist_item_append(gl, ttc, NULL, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);

   for (i = 0; i < N_ITEMS; i++)
     {
        if (i == 999)
          target_item = elm_genlist_item_append(gl, itc, (void *)i, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);
        else elm_genlist_item_append(gl, itc, (void *)i, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);
     }

   elm_genlist_item_append(gl, ptc, NULL, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);

   elm_genlist_item_class_free(ttc);
   elm_genlist_item_class_free(itc);
   elm_genlist_item_class_free(ptc);

   evas_object_smart_callback_add(gl, "loaded", _start_calc, (void *)pd);
   evas_object_show(gl);

   Elm_Object_Item * nvit = elm_naviframe_item_push(ad->nf, NULL, NULL, NULL, gl, "empty");

   elm_naviframe_item_pop_cb_set(nvit, _nf_pop_cb, pd);
}

