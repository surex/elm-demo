/*
 * Copyright (c) 2014 Samsung Electronics Co., Ltd All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
#include <app_manager.h>
#include <app_control.h>
#include "main.h"


static Eina_Bool screen_reader_on = EINA_FALSE;
Evas_Object *screen_reader_onoff_check;

static bool is_screen_reader_running()
{
	int screen_reader = 0;
	vconf_get_bool(VCONFKEY_SETAPPL_ACCESSIBILITY_TTS, &screen_reader);
	return screen_reader;
}

void launch_reply_callback(app_control_h request, app_control_h reply, app_control_result_e result, void *user_data)
{
	dlog_print(DLOG_DEBUG, LOG_TAG, "reply has been delivered");
}

void
screen_reader_launch(void)
{
	vconf_set_bool(VCONFKEY_SETAPPL_ACCESSIBILITY_TTS, 1);
}

void _utterance_completed_cb(void *data, const char *say_signal)
{
	vconf_set_bool(VCONFKEY_SETAPPL_ACCESSIBILITY_TTS, 0);
}

void
screen_reader_terminate(void)
{
	elm_atspi_bridge_utils_say("Screen reader off", EINA_FALSE, _utterance_completed_cb, NULL);
}

static void
screen_reader_item_cb(void *data, Evas_Object *obj, void *event_info)
{
	Eina_Bool check_state = elm_check_state_get(screen_reader_onoff_check);

	elm_check_state_set(screen_reader_onoff_check, !check_state);
	screen_reader_on = !check_state;

	if(screen_reader_on)
		screen_reader_launch();
	else
		screen_reader_terminate();
}

static void
onoff_check_changed_cb(void *data, Evas_Object *obj, void *event_info)
{
	screen_reader_on = elm_check_state_get(obj);

	if(screen_reader_on == EINA_TRUE)
		screen_reader_launch();
	else
		screen_reader_terminate();
}

static char *
_gl_menu_title_text_get(void *data, Evas_Object *obj, const char *part)
{
	char buf[1024];

	snprintf(buf, 1023, "Screen Reader");
	return strdup(buf);
}

static char *
_gl_menu_text_get(void *data, Evas_Object *obj, const char *part)
{
	char buf[1024];

	if (!strcmp(part, "elm.text")) {
		snprintf(buf, 1023, "Screen Reader(TTS)");
		return strdup(buf);
	}
	return NULL;
}

static Evas_Object *
_gl_icon_get(void *data, Evas_Object *obj, const char *part)
{
    if (strcmp(part, "elm.icon")) return NULL;

	screen_reader_onoff_check = elm_check_add(obj);
	elm_object_style_set(screen_reader_onoff_check, "on&off");
	evas_object_smart_callback_add(screen_reader_onoff_check, "changed", onoff_check_changed_cb, NULL);
	evas_object_propagate_events_set(screen_reader_onoff_check, EINA_FALSE);
    evas_object_show(screen_reader_onoff_check);
	screen_reader_on = is_screen_reader_running();
	elm_check_state_set(screen_reader_onoff_check, screen_reader_on);

    return screen_reader_onoff_check;
}

void
screen_reader_cb(void *data, Evas_Object *obj, void *event_info)
{
	appdata_s *ad = NULL;
	Evas_Object *genlist = NULL;
	Evas_Object *circle_genlist;

	Elm_Genlist_Item_Class *itc = elm_genlist_item_class_new();
	Elm_Genlist_Item_Class *ttc = elm_genlist_item_class_new();

	ad = (appdata_s *) data;
	if (ad == NULL) return;

	genlist = elm_genlist_add(ad->nf);
	elm_genlist_mode_set(genlist, ELM_LIST_COMPRESS);

	circle_genlist = eext_circle_object_genlist_add(genlist, ad->circle_surface);
	eext_circle_object_genlist_scroller_policy_set(circle_genlist, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
	eext_rotary_object_event_activated_set(circle_genlist, EINA_TRUE);

	ttc->item_style = "title";
	ttc->func.text_get = _gl_menu_title_text_get;
	elm_genlist_item_append(genlist, ttc, NULL, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);

	itc->item_style = "1text.1icon.1";
	itc->func.text_get = _gl_menu_text_get;
	itc->func.content_get = _gl_icon_get;
	elm_genlist_item_append(genlist, itc, NULL, NULL, ELM_GENLIST_ITEM_NONE, screen_reader_item_cb, NULL);

	elm_genlist_item_class_free(itc);
	elm_genlist_item_class_free(ttc);

	elm_naviframe_item_push(ad->nf, NULL, NULL, NULL, genlist, "empty");
}
