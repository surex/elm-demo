/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

#include "main.h"

static Evas_Object*
create_scroller(appdata_s *ad)
{
	Evas_Object *scroller = elm_scroller_add(ad->nf);
	elm_scroller_bounce_set(scroller, EINA_FALSE, EINA_TRUE);
	elm_scroller_policy_set(scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
	evas_object_show(scroller);

	Evas_Object *circle_scroller = eext_circle_object_scroller_add(scroller, ad->circle_surface);
	eext_circle_object_scroller_policy_set(circle_scroller, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
	eext_rotary_object_event_activated_set(circle_scroller, EINA_TRUE);

	return scroller;
}

static Evas_Object*
create_layout(Evas_Object *parent)
{
	Evas_Object *layout;

	layout = elm_layout_add(parent);
	elm_layout_file_set(layout, ELM_DEMO_EDJ, "reading_order_layout");
	evas_object_size_hint_weight_set(layout, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);

	return layout;
}

static Evas_Object*
create_layout_button(Evas_Object *layout, const char* label, const char* part_name)
{
	Evas_Object *button;

	button = elm_button_add(layout);
	elm_object_text_set(button, label);
	elm_object_part_content_set(layout, part_name, button);

	return button;
}

static Evas_Object*
create_reading_order(Evas_Object *parent)
{
	Evas_Object *layout;

	Evas_Object *button_a, *button_b, *button_c;
	Evas_Object *button_d, *button_e, *button_f;

	layout = create_layout(parent);

	button_a = create_layout_button(layout, "a", "button_a");
	button_b = create_layout_button(layout, "b", "button_b");
	button_c = create_layout_button(layout, "c", "button_c");
	button_d = create_layout_button(layout, "d", "button_d");
	button_e = create_layout_button(layout, "e", "button_e");
	button_f = create_layout_button(layout, "f", "button_f");

	elm_atspi_accessible_relationship_append(button_a, ELM_ATSPI_RELATION_FLOWS_TO, button_d);
	elm_atspi_accessible_relationship_append(button_d, ELM_ATSPI_RELATION_FLOWS_TO, button_e);
	elm_atspi_accessible_relationship_append(button_e, ELM_ATSPI_RELATION_FLOWS_TO, button_f);
	elm_atspi_accessible_relationship_append(button_f, ELM_ATSPI_RELATION_FLOWS_TO, button_c);

	elm_atspi_accessible_relationship_append(button_c, ELM_ATSPI_RELATION_FLOWS_FROM, button_f);
	elm_atspi_accessible_relationship_append(button_f, ELM_ATSPI_RELATION_FLOWS_FROM, button_e);
	elm_atspi_accessible_relationship_append(button_e, ELM_ATSPI_RELATION_FLOWS_FROM, button_d);
	elm_atspi_accessible_relationship_append(button_d, ELM_ATSPI_RELATION_FLOWS_FROM, button_a);

	evas_object_show(layout);

	return layout;
}

void
reading_order_cb(void *data, Evas_Object *obj, void *event_info)
{
	appdata_s *ad = (appdata_s *)data;
	Evas_Object *scroller, *layout;
	Evas_Object *nf = ad->nf;

	scroller = create_scroller(ad);
	layout = create_reading_order(scroller);

	elm_object_content_set(scroller, layout);

	elm_naviframe_item_push(nf, "Reading Order", NULL, NULL, scroller, "empty");
}
