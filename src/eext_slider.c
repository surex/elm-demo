/*
 * Copyright (c) 2014 Samsung Electronics Co., Ltd All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

#include "main.h"

static void
_value_changed(void *data, Evas_Object *obj, void *event_info)
{
	char buf[PATH_MAX];
	Evas_Object *layout = (Evas_Object *)data;

	snprintf(buf, sizeof(buf), "%.0lf", eext_circle_object_value_get(obj));
	printf("Slider value = %s\n", buf);
	elm_object_part_text_set(layout, "elm.text.slider", buf);
}

static void
_button_decrease_cb(void *data, Evas_Object *obj, void *event_info)
{
	Evas_Object *slider = data;
	double min;

	double value = eext_circle_object_value_get(slider);
	double step = eext_circle_object_slider_step_get(slider);
	eext_circle_object_value_min_max_get(slider, &min, NULL);

	if (value - step >= 0)
	  eext_circle_object_value_set(slider, value - step);
}

static void
_button_increase_cb(void *data, Evas_Object *obj, void *event_info)
{
	Evas_Object *slider = data;
	double max;

	double value = eext_circle_object_value_get(slider);
	double step = eext_circle_object_slider_step_get(slider);
	eext_circle_object_value_min_max_get(slider, NULL, &max);

	if (value + step <= max)
	  eext_circle_object_value_set(slider, value + step);
}

void
eext_slider_cb(void *data, Evas_Object *obj, void *event_info EINA_UNUSED)
{
	appdata_s *ad = (appdata_s *)data;
	Evas_Object *nf = ad->nf;
	Evas_Object *layout = NULL;
	Evas_Object *slider = NULL;
	Evas_Object *button = NULL;

	layout = elm_layout_add(nf);
	elm_layout_file_set(layout, ELM_DEMO_EDJ, "eext_slider_layout");
	elm_object_part_text_set(layout, "elm.text.slider", "5");
	evas_object_show(layout);

	/* Add new eext_circle_object_slider with Eext_Circle_Surface object to render.
	Value is set as 5 which is within range from 1 to 10. */
	slider = eext_circle_object_slider_add(layout, ad->circle_surface);
	eext_circle_object_value_min_max_set(slider, 1.0, 10.0);
	eext_circle_object_value_set(slider, 5.0);

	/* Activate Circle slider's rotary object event.
	Its value increases/decreases its value by 1.0 to clockwise/counter clockwise
	rotary event. */
	eext_rotary_object_event_activated_set(slider, EINA_TRUE);
	eext_circle_object_slider_step_set(slider, 1.0);
	evas_object_smart_callback_add(slider, "value,changed", _value_changed, layout);

	button = elm_button_add(layout);
	elm_object_text_set(button, "-");
	evas_object_smart_callback_add(button, "clicked", _button_decrease_cb, slider);
	elm_object_part_content_set(layout, "elm.swallow.button.left", button);

	button = elm_button_add(layout);
	elm_object_text_set(button, "+");
	evas_object_smart_callback_add(button, "clicked", _button_increase_cb, slider);
	elm_object_part_content_set(layout, "elm.swallow.button.right", button);

	elm_object_part_text_set(layout, "elm.text.slider.bottom_1", "Slider name");
	elm_object_part_text_set(layout, "elm.text.slider.bottom_2", "Slider name sub");

	elm_naviframe_item_push(nf, _("Slider"), NULL, NULL, layout, "empty");
}
