/*
 * Copyright (c) 2011 Samsung Electronics Co., Ltd All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

#include "main.h"

typedef struct _Progressbar_Data {
	Ecore_Timer *p_timer_custom;
	Ecore_Timer *p_timer_small;
	Ecore_Timer *p_timer_full;
	Evas_Object *layout;
	Evas_Object *progressbar_custom;
	Evas_Object *progressbar_small;
	Evas_Object *progressbar_full;
} Progressbar_Data;

static Eina_Bool
_progressbar_timer_cb_custom(void *data)
{
	double progress_custom;
	char buf[PATH_MAX];
	Progressbar_Data *pd = (Progressbar_Data *)data;

	/* Get current progress value to increase */
	progress_custom = eext_circle_object_value_get(pd->progressbar_custom);
	progress_custom += 5.0;

	if (progress_custom > 100)
		progress_custom = 0.0;

	/* Set increased value as new progress */
	eext_circle_object_value_set(pd->progressbar_custom, progress_custom);

	snprintf(buf, sizeof(buf), "%d %%", (int) progress_custom);
	elm_object_part_text_set(pd->layout, "elm.text.progressbar.custom", buf);

	return ECORE_CALLBACK_RENEW;
}

static Eina_Bool
_progressbar_timer_cb_small(void *data)
{
	double progress_small;
	char buf[PATH_MAX];
	Progressbar_Data *pd = (Progressbar_Data *)data;

	/* Get current progress value to increase */
	progress_small = eext_circle_object_value_get(pd->progressbar_small);
	progress_small += 5.0;

	if (progress_small > 100)
		progress_small = 0.0;

	/* Set increased value as new progress */
	eext_circle_object_value_set(pd->progressbar_small, progress_small);

	snprintf(buf, sizeof(buf), "%d %%", (int) progress_small);
	elm_object_part_text_set(pd->layout, "elm.text.progressbar.small", buf);

	return ECORE_CALLBACK_RENEW;
}

static Eina_Bool
_progressbar_timer_cb_full(void *data)
{
	double progress_full;
	char buf[PATH_MAX];
	Progressbar_Data *pd = (Progressbar_Data *)data;

	/* Get current progress value to increase */
	progress_full = eext_circle_object_value_get(pd->progressbar_full);
	progress_full += 5.0;

	if (progress_full > 100)
		progress_full = 0.0;

	/* Set increased value as new progress */
	eext_circle_object_value_set(pd->progressbar_full, progress_full);

	snprintf(buf, sizeof(buf), "%d %%", (int) progress_full);
	elm_object_part_text_set(pd->layout, "elm.text.progressbar.full", buf);

	return ECORE_CALLBACK_RENEW;
}

static Eina_Bool
_naviframe_item_pop_cb(void *data, Elm_Object_Item *it)
{
	Progressbar_Data *pd = (Progressbar_Data *)data;

	if (pd->p_timer_custom) {
		ecore_timer_del(pd->p_timer_custom);
		pd->p_timer_custom = NULL;
	}
	if (pd->p_timer_small) {
		ecore_timer_del(pd->p_timer_small);
		pd->p_timer_small = NULL;
	}
	if (pd->p_timer_full) {
		ecore_timer_del(pd->p_timer_full);
		pd->p_timer_full = NULL;
	}

	return EINA_TRUE;
}

void
eext_progressbar_cb(void *data, Evas_Object *obj, void *event_info)
{
	appdata_s *ad = (appdata_s *)data;
	Evas_Object *nf = ad->nf;
	static Progressbar_Data pd = { NULL, };
	Evas_Object *layout = NULL;
	Evas_Object *progressbar_custom = NULL;
	Evas_Object *progressbar_small = NULL;
	Evas_Object *progressbar_full = NULL;
	Elm_Object_Item *nf_it = NULL;

	pd.layout = layout = elm_layout_add(nf);
	elm_layout_file_set(layout, ELM_DEMO_EDJ, "eext_progressbar_layout");
	evas_object_show(layout);

	/* Add new eext_circle_object_progressbar with Eext_Circle_Surface argument as NULL.
	Since the circle_object is not connected to Eext_Circle_Surface object,
	it will be rendered independently along with Evas_Object and set as a content of layout. */
	pd.progressbar_custom = progressbar_custom = eext_circle_object_progressbar_add(layout, NULL);
	elm_object_part_content_set(layout, "elm.swallow.content", progressbar_custom);

	/* Set the circle object's properties.
	- circle object's value has a range from 0.0 to 100.0
	- circle object has a shape of Red(R: 255, G:0, B: 0, A: 0)
	and 4-width circle line with radius of 100 */
	eext_circle_object_value_min_max_set(progressbar_custom, 0.0, 100.0);
	eext_circle_object_radius_set(progressbar_custom, 100);
	eext_circle_object_color_set(progressbar_custom, 255, 0, 0, 255);
	eext_circle_object_line_width_set(progressbar_custom, 8);
	eext_circle_object_item_radius_set(progressbar_custom, "bg", 100);
	eext_circle_object_item_color_set(progressbar_custom, "bg", 0, 255, 0, 255);
	eext_circle_object_item_line_width_set(progressbar_custom, "bg", 8);

	evas_object_show(progressbar_custom);

	pd.p_timer_custom = ecore_timer_add(0.2, _progressbar_timer_cb_custom, &pd);

	/* Add new eext_circle_object_progressbar.
	- progress indicator small */
	pd.progressbar_small = progressbar_small = eext_circle_object_progressbar_add(layout, NULL);
	elm_object_part_content_set(layout, "elm.swallow.progressbar.small", progressbar_small);
	/* Set the circle object's properties. */
	eext_circle_object_value_min_max_set(progressbar_small, 0.0, 100.0);
	eext_circle_object_radius_set(progressbar_small, 26);
	eext_circle_object_line_width_set(progressbar_small, 4);
	eext_circle_object_item_radius_set(progressbar_small, "bg", 26);
	eext_circle_object_item_line_width_set(progressbar_small, "bg", 4);

	evas_object_show(progressbar_small);

	pd.p_timer_small = ecore_timer_add(0.1, _progressbar_timer_cb_small, &pd);

	/* Add new eext_circle_object_progressbar.
	Since the circle_object is connected to Eext_Circle_Surface object,
	it will be rendered by Eext_Circle_Surface object */
	pd.progressbar_full = progressbar_full = eext_circle_object_progressbar_add(layout, ad->circle_surface);

	/* Set the circle object's properties.
	- circle object's value has a range from 0.0 to 100.0 */
	eext_circle_object_value_min_max_set(progressbar_full, 0.0, 100.0);
	evas_object_show(progressbar_full);

	pd.p_timer_full = ecore_timer_add(0.1, _progressbar_timer_cb_full, &pd);

	nf_it = elm_naviframe_item_push(nf, _("Progress Fullscreen"), NULL, NULL, layout, "empty");
	elm_naviframe_item_title_enabled_set(nf_it, EINA_FALSE, EINA_FALSE);
	elm_naviframe_item_pop_cb_set(nf_it, _naviframe_item_pop_cb, &pd);
}
