/*
 * Copyright (c) 2011 Samsung Electronics Co., Ltd All Rights Reserved
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

#include "main.h"

#define NUM_OF_ITEMS 101
#define NUM_OF_GENLIST_DEMO_NAMES 101
#define NUM_OF_GENLIST_DEMO_LONG_TEXT 7

static char *menu_its[] = {
	/*** 1line styles ***/
	"List: Seamless Effect",
	"List: Seamless Effect From Button",
	"List: Go to Top",
	"title",
	"title_with_groupindex",
	"1text",
	"1text.1icon",
	"1text.1icon.1",
	"1text.1icon.divider",
	"2text",
	"2text.1icon",
	"2text.1icon.1",
	"2text.1icon.divider",
	"3text",
	"4text",
	"editfield",
	"multiline",
	"select_mode",
	/* do not delete below */
	NULL
};

char *eext_genlist_demo_names[] = {
	"Aaliyah", "Aamir", "Aaralyn", "Aaron", "Abagail",
	"Babitha", "Bahuratna", "Bandana", "Bulbul", "Cade", "Caldwell",
	"CaptainFantasticFasterThanSupermanSpidermanBatmanWolverineHulkAndTheFlashCombined",
	"Chandan", "Caster", "Dagan ", "Daulat", "Dag", "Earl", "Ebenzer",
	"Ellison", "Elizabeth", "Filbert", "Fitzpatrick", "Florian", "Fulton",
	"Frazer", "Gabriel", "Gage", "Galen", "Garland", "Gauhar", "Hadden",
	"Hafiz", "Hakon", "Haleem", "Hank", "Hanuman", "Jabali ", "Jaimini",
	"Jayadev", "Jake", "Jayatsena", "Jonathan", "Kamaal", "Jeirk",
	"Jasper", "Jack", "Mac", "Macy", "Marlon", "Milson",
	"Aaliyah", "Aamir", "Aaralyn", "Aaron", "Abagail",
	"Babitha", "Bahuratna", "Bandana", "Bulbul", "Cade", "Caldwell",
	"Chandan", "Caster", "Dagan ", "Daulat", "Dag", "Earl", "Ebenzer",
	"Ellison", "Elizabeth", "Filbert", "Fitzpatrick", "Florian", "Fulton",
	"Frazer", "Gabriel", "Gage", "Galen", "Garland", "Gauhar", "Hadden",
	"Hafiz", "Hakon", "Haleem", "Hank", "Hanuman", "Jabali ", "Jaimini",
	"Jayadev", "Jake", "Jayatsena", "Jonathan", "Kamaal", "Jeirk",
	"Jasper", "Jack", "Mac", "Macy", "Marlon", "Milson",
	NULL
};

char *eext_genlist_demo_names2 = "GenlistDemoLongTitle";

char *eext_long_txt[] = {
	"Hey John, how have you been?",
	"Andy, it's been a long time, how are you man?",
	"I finally have some free time. I just finished taking a big examination, and I'm so relieved that I'm done with it",
	"Wow. How long has it been? It seems like more than a year. I'm doing pretty well. How about you?",
	"I'm playing a video game on my computer because I have nothing to do.",
	"I'm pretty busy right now. I'm doing my homework because I have an exam tomorrow.",
	"I'm taking the day off from work today because I have so many errands. I'm going to the post office to send some packages to my friends.",
	NULL
};

typedef struct _genlist_view_data {
	appdata_s* ad;
	Evas_Object *btn;
	Evas_Object *layout;
	Evas_Object *ctxpopup;
	unsigned int check_cnt;
	Eina_Bool is_select_mode;
	int index;
	Evas_Object *main_genlist;
	Evas_Object *genlist;
	Elm_Object_Item *aligned_item;
} genlist_view_data;

typedef struct _item_data {
	genlist_view_data *gvd;
	int index;
	Elm_Object_Item *item;
	Evas_Object *check;
	Eina_Bool checked;
	Eina_Bool disabled;
	Evas_Object *perspective_obj;
} item_data;

static void _create_genlist(void *, Evas_Object *, void *);

static void
gl_selected_cb(void *data, Evas_Object *obj, void *event_info)
{
	Elm_Object_Item *it = (Elm_Object_Item *)event_info;
	elm_genlist_item_selected_set(it, EINA_FALSE);
}

static char *
_gl_menu_title_text_get(void *data, Evas_Object *obj, const char *part)
{
	char buf[1024];

	snprintf(buf, 1023, "%s", "Eext Genlist");
	return strdup(buf);
}

static char *
_gl_menu_text_get(void *data, Evas_Object *obj, const char *part)
{
	char buf[1024];
	item_data *id = (item_data *)data;
	int index = id->index;

	if (!strcmp(part, "elm.text")) {
		snprintf(buf, 1023, "%s", menu_its[index]);
		return strdup(buf);
	}
	return NULL;
}

static void
_gl_menu_del(void *data, Evas_Object *obj)
{
	// FIXME: Unrealized callback can be called after this.
	// Accessing item_data can be dangerous on unrealized callback.
	item_data *id = (item_data *)data;
	if (id) free(id);
}

static void
_gl_del(void *data, Evas_Object *obj)
{
	/* FIXME: Unrealized callback can be called after this.
	Accessing item_data can be dangerous on unrealized callback. */
	item_data *id = (item_data *)data;
	if (id) free(id);
}

static void
_gl_ctxpopup_del_cb(void *data, Evas *e, Evas_Object *obj, void *event_info)
{
	genlist_view_data *gvd = (genlist_view_data *)data;
	if (gvd) free(gvd);
}

static void
_gl_icon_clicked_cb(void *data, Evas_Object *obj, void *event_info)
{
	int param = (int) data;

	Eina_Bool state = elm_check_state_get(obj);
	printf("Check %d:%d\n", param, state);
}

static Evas_Object*
_gl_icon_get(void *data, Evas_Object *obj, const char *part)
{
	item_data *id = (item_data *)data;
	Evas_Object *content = NULL;

	if (strcmp(part, "elm.icon")) return NULL;

	content = elm_check_add(obj);
	elm_check_state_set(content, EINA_FALSE);
	evas_object_smart_callback_add(content, "changed", _gl_icon_clicked_cb, (void*)(id->index));
	elm_check_state_pointer_set(content, &id->checked);
	evas_object_show(content);

	return content;
}

static void
_gl_check_clicked_cb(void *data, Evas_Object *obj, void *event_info)
{
	item_data *id = (item_data *)data;
	char buf[5];
	id->gvd->check_cnt += (id->checked) ? 1 : -1;
	snprintf(buf, 5, "%.2d", id->gvd->check_cnt);
	elm_object_text_set(id->gvd->btn, buf);
}

static void
_gl_longpress_cb(void *data, Evas_Object *obj, void *event_info)
{
	item_data *id = NULL;
	Elm_Object_Item *it = (Elm_Object_Item *)event_info;
	genlist_view_data *gvd = (genlist_view_data *)data;
	elm_genlist_item_selected_set(it, EINA_FALSE);

	if (!gvd->is_select_mode) {
		gvd->is_select_mode = EINA_TRUE;
		id = elm_object_item_data_get(it);
		id->checked = EINA_TRUE;
		elm_genlist_realized_items_update(obj);

		char buf[5];
		gvd->check_cnt++;
		snprintf(buf, 5, "%.2d", gvd->check_cnt);
		elm_object_text_set(gvd->btn, buf);
		elm_object_signal_emit(gvd->layout, "select_mode,button,show", "");
	}
}

static Evas_Object*
_gl_check_get(void *data, Evas_Object *obj, const char *part)
{
	item_data *id = (item_data *)data;

	if ((!id->gvd->is_select_mode) || strcmp(part, "elm.swallow.center_check")) return NULL;

	id->check = elm_check_add(obj);
	elm_object_style_set(id->check, "genlist/select_mode");
	elm_check_state_set(id->check, EINA_FALSE);
	evas_object_smart_callback_add(id->check, "changed", _gl_check_clicked_cb, data);
	elm_check_state_pointer_set(id->check, &id->checked);
	evas_object_repeat_events_set(id->check, EINA_FALSE);
	evas_object_propagate_events_set(id->check, EINA_FALSE);
	evas_object_show(id->check);

	return id->check;
}

static Evas_Object*
_gl_divider_icon_get(void *data, Evas_Object *obj, const char *part)
{
	Evas_Object *btn = NULL;
	Evas_Object *ic = NULL;

	if (strcmp(part, "elm.icon")) return NULL;

	btn = elm_button_add(obj);
	elm_object_style_set(btn, "list_divider_btn");
	evas_object_size_hint_weight_set(btn, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(btn, EVAS_HINT_FILL, EVAS_HINT_FILL);

	ic = elm_image_add(btn);
	elm_image_file_set(ic, ICON_DIR "/tw_btn_delete_holo_dark.png", NULL);
	elm_object_content_set(btn, ic);
	evas_object_propagate_events_set(btn, EINA_FALSE);

	return btn;
}

static char*
_gl_text_get(void *data, Evas_Object *obj, const char *part)
{
	char buf[1024];
	item_data *id = (item_data *)data;
	int index = id->index;

	if (!strcmp(part, "elm.text.1") || !strcmp(part, "elm.text.2")) index++;

	snprintf(buf, 1023, "%s:%s", part, eext_genlist_demo_names[index%NUM_OF_GENLIST_DEMO_NAMES]);

	return strdup(buf);
}

static char*
_gl_title_text_get(void *data, Evas_Object *obj, const char *part)
{
	char buf[1024];

	snprintf(buf, 1023, "%s:%s", part, eext_genlist_demo_names2);

	return strdup(buf);
}

static char *
_gl_long_text_get(void *data, Evas_Object *obj, const char *part)
{
	char buf[1024];
	item_data *id = (item_data *)data;
	int index = id->index;

	snprintf(buf, 1023, "%s:%s", part, eext_long_txt[index%NUM_OF_GENLIST_DEMO_LONG_TEXT]);
	return strdup(buf);
}

static void
_gl_sel(void *data, Evas_Object *obj, void *event_info)
{
	Elm_Object_Item *it = (Elm_Object_Item *)event_info;
	elm_genlist_item_selected_set(it, EINA_FALSE);

	return;
}

static void
_gl_select_mode_sel(void *data, Evas_Object *obj, void *event_info)
{
	item_data *id = NULL;
	Elm_Object_Item *it = (Elm_Object_Item *)event_info;
	char buf[5];

	genlist_view_data *gvd = (genlist_view_data *)data;
	elm_genlist_item_selected_set(it, EINA_FALSE);

	if (!gvd->is_select_mode) return;

	id = elm_object_item_data_get(it);
	id->checked = !id->checked;
	elm_check_state_set(id->check, id->checked);
	gvd->check_cnt += (id->checked) ? 1 : -1;
	snprintf(buf, 5, "%.2d", gvd->check_cnt);
	elm_object_text_set(gvd->btn, buf);
}

static void
_gl_ctxpopup_select_all_cb(void *data, Evas_Object *obj, void *event_info)
{
	char buf[5];

	genlist_view_data *gvd = (genlist_view_data *)data;
	Evas_Object *layout = gvd->layout;
	Evas_Object *genlist = elm_layout_content_get(layout, "elm.swallow.content");
	Evas_Object *select_btn = gvd->btn;
	Evas_Object *ctxpopup = gvd->ctxpopup;
	Elm_Object_Item *it = elm_genlist_first_item_get(genlist);

	while (it) {
		item_data *id = elm_object_item_data_get(it);
		if (!id) {
			it = elm_genlist_item_next_get(it);
			continue;
		}
		/* For realized items, set state of real check object */
		Evas_Object *ck = elm_object_item_part_content_get(it, "elm.swallow.center_check");
		if (ck) elm_check_state_set(ck, EINA_TRUE);
		/* For all items (include unrealized), just set pointer state */
		id->checked = EINA_TRUE;
		it = elm_genlist_item_next_get(it);
	}
	// The return value of the elm_genlist_items_count = List item + Padding item
	gvd->check_cnt = elm_genlist_items_count(genlist) - 2;
	snprintf(buf, 5, "%.2d", gvd->check_cnt);
	elm_object_text_set(select_btn, buf);

	if (ctxpopup) elm_ctxpopup_dismiss(ctxpopup);
}

static void
_gl_ctxpopup_deselect_all_cb(void *data, Evas_Object *obj, void *event_info)
{
	char buf[5];

	genlist_view_data *gvd = (genlist_view_data *)data;
	Evas_Object *layout = gvd->layout;
	Evas_Object *genlist = elm_layout_content_get(layout, "elm.swallow.content");
	Evas_Object *select_btn = gvd->btn;
	Evas_Object *ctxpopup = gvd->ctxpopup;

	Elm_Object_Item *it = elm_genlist_first_item_get(genlist);
	while (it) {
		item_data *id = elm_object_item_data_get(it);
		if (!id) {
			it = elm_genlist_item_next_get(it);
			continue;
		}
		/* For realized items, set state of real check object */
		Evas_Object *ck = elm_object_item_part_content_get(it, "elm.swallow.center_check");
		if (ck) elm_check_state_set(ck, EINA_FALSE);
		/* For all items (include unrealized), just set pointer state */
		id->checked = EINA_FALSE;
		it = elm_genlist_item_next_get(it);
	}
	gvd->check_cnt = 0;
	snprintf(buf, 5, "%.2d", gvd->check_cnt);
	elm_object_text_set(select_btn, buf);

	if (ctxpopup) elm_ctxpopup_dismiss(ctxpopup);
}

static void
_gl_dismissed_cb(void *data, Evas_Object *obj, void *event_info)
{
	Evas_Object *ctxpopup = (Evas_Object *)data;

	evas_object_smart_callback_del(ctxpopup, "dismissed", _gl_dismissed_cb);
	evas_object_del(ctxpopup);
}

static void
_gl_move_ctxpopup(Evas_Object *ctxpopup, Evas_Object *btn)
{
	Evas_Coord x, y, w, h;

	evas_object_geometry_get(btn, &x, &y, &w, &h);
	evas_object_move(ctxpopup, x + (w / 2), y + (h /2));
}

static void
_create_ctxpopup_action_button(void *data, Evas_Object *obj, void *event_info)
{
	genlist_view_data *gvd = (genlist_view_data *)data;
	Evas_Object *layout = gvd->layout;
	Evas_Object *ctxpopup = NULL;
	Evas_Object *genlist = NULL;
	Elm_Object_Item *eo_it1 = NULL;
	Elm_Object_Item *eo_it2 = NULL;

	ctxpopup = elm_ctxpopup_add(layout);
	elm_object_style_set(ctxpopup, "select_mode");
	elm_ctxpopup_direction_priority_set(ctxpopup, ELM_CTXPOPUP_DIRECTION_DOWN,
						ELM_CTXPOPUP_DIRECTION_DOWN,
						ELM_CTXPOPUP_DIRECTION_DOWN,
						ELM_CTXPOPUP_DIRECTION_DOWN);
	evas_object_smart_callback_add(ctxpopup, "dismissed", _gl_dismissed_cb, ctxpopup);

	genlist = elm_layout_content_get(layout, "elm.swallow.content");
	gvd->layout = layout;
	gvd->ctxpopup = ctxpopup;

	if (gvd->check_cnt < (elm_genlist_items_count(genlist) - 2))
		eo_it1 = elm_ctxpopup_item_append(ctxpopup, _("Select all"), NULL, _gl_ctxpopup_select_all_cb, gvd);
	if (gvd->check_cnt)
		eo_it2 = elm_ctxpopup_item_append(ctxpopup, _("Deselect all"), NULL, _gl_ctxpopup_deselect_all_cb, gvd);

	evas_object_show(ctxpopup);
	if (eo_it1 && eo_it2) {
		elm_object_item_style_set(eo_it1, "select_mode/top");
		elm_object_item_style_set(eo_it2, "select_mode/bottom");
	}
	_gl_move_ctxpopup(ctxpopup, obj);
}

static Evas_Object*
_create_select_mode_genlist(genlist_view_data *gvd)
{
	char buf[5];
	Evas_Object * genlist = NULL;

	gvd->layout = elm_layout_add(gvd->ad->nf);
	elm_layout_theme_set(gvd->layout, "layout", "select_mode", "default");
	evas_object_show(gvd->layout);
	genlist = elm_genlist_add(gvd->layout);
	elm_object_style_set(genlist, "focus_bg");
	elm_layout_content_set(gvd->layout, "elm.swallow.content", genlist);

	evas_object_smart_callback_add(genlist, "longpressed", _gl_longpress_cb, gvd);
	elm_genlist_longpress_timeout_set(genlist, 1.0);

	gvd->btn = elm_button_add(gvd->layout);
	elm_object_style_set(gvd->btn, "select_mode");
	snprintf(buf, 5, "%.2d", gvd->check_cnt);
	elm_object_text_set(gvd->btn, buf);
	evas_object_smart_callback_add(gvd->btn, "clicked", _create_ctxpopup_action_button, gvd);

	elm_layout_content_set(gvd->layout, "elm.swallow.icon", gvd->btn);

	return genlist;
}

static Eina_Bool
_nf_pop_cb(void *data, Elm_Object_Item *it)
{
	Evas_Object *genlist = (Evas_Object *)data;

	evas_object_hide(genlist);

	return EINA_TRUE;
}

static void
_back_to_top_cb(void *data, Evas_Object *obj, void *event_info)
{
	elm_genlist_item_bring_in(elm_genlist_first_item_get(obj), ELM_GENLIST_ITEM_SCROLLTO_TOP);
}

static void
__genlist_delete_cb(void *data, Evas *e, Evas_Object *obj, void *event_info)
{
	genlist_view_data *gvd = (genlist_view_data *) data;
	if (gvd)
	{
		free(gvd);
	}
}

static void
__aligned_item_detected_cb(void *data, Evas_Object *obj, void *event_info)
{
	if (!event_info) return;

	genlist_view_data *gvd = data;
	gvd->aligned_item = event_info;
	item_data *id = elm_object_item_data_get(gvd->aligned_item);
	if (id->perspective_obj
		&& !strcmp(menu_its[id->index], "List: Seamless Effect From Button"))
	{
		_create_genlist(id, obj, event_info);
	}
}

static void
__seamless_button_clicked_cb(void *data, Evas_Object *obj, void *event_info EINA_UNUSED)
{
	item_data *id = data;

	if (id->gvd->aligned_item == id->item)
	{
		id->perspective_obj = obj;
		_create_genlist(id, id->gvd->main_genlist, id->item);
	}
	else
	{
		id->perspective_obj = obj;
		elm_genlist_item_bring_in(id->item, ELM_GENLIST_ITEM_SCROLLTO_MIDDLE);
	}
}

static Evas_Object *
__genlist_item_seamless_content_get_cb(void *data, Evas_Object *obj, const char *part)
{
	Evas_Object *layout = NULL;

	if (strcmp(part, "elm.swallow.content")) return NULL;

	item_data *id = data;
	layout = elm_layout_add(obj);
	elm_layout_file_set(layout, ELM_DEMO_EDJ, "genlist/2button");
	evas_object_show(layout);

	Evas_Object *button1 = elm_button_add(obj);
	elm_object_style_set(button1, "circle");
	evas_object_smart_callback_add(button1, "clicked", __seamless_button_clicked_cb, id);

	Evas_Object *circle_icon = elm_image_add(button1);

	elm_image_file_set(circle_icon, ICON_DIR "/g1.png", NULL);

	evas_object_size_hint_min_set(circle_icon, 100, 100);
	evas_object_show(circle_icon);
	elm_object_part_content_set(button1, "elm.swallow.content", circle_icon);

	Evas_Object *button2 = elm_button_add(obj);
	elm_object_style_set(button2, "circle");
	evas_object_smart_callback_add(button2, "clicked", __seamless_button_clicked_cb, id);

	circle_icon = elm_image_add(button2);

	elm_image_file_set(circle_icon, ICON_DIR "/g2.png", NULL);
	evas_object_size_hint_min_set(circle_icon, 100, 100);
	evas_object_show(circle_icon);
	elm_object_part_content_set(button2, "elm.swallow.content", circle_icon);

	elm_object_part_content_set(layout, "button1", button1);
	elm_object_part_content_set(layout, "button2", button2);
	return layout;
}

static Evas_Object *
__genlist_item_content_get_cb(void *data, Evas_Object *obj, const char *part)
{
	item_data *id = (item_data *)data;
	Evas_Object *content = NULL;

	if (strcmp(part, "elm.icon")) return NULL;
	content = elm_image_add(obj);
	elm_image_file_set(content, ICON_DIR "/g1.png", NULL);
	evas_object_size_hint_min_set(content, 70, 70);
	evas_object_size_hint_max_set(content, 70, 70);
	id->perspective_obj = content;

	evas_object_show(content);
	return content;
}
static void
_create_genlist(void *data, Evas_Object *obj, void *event_info)
{
	if (!data || !event_info) return;

	item_data *id = (item_data *)data;
	genlist_view_data *gvd = id->gvd;
	appdata_s *ad = id->gvd->ad;
	int index = 0;
	Elm_Object_Item *it = (Elm_Object_Item *)event_info;
	Elm_Object_Item *item = NULL;
	Elm_Object_Item *nf_it;
	Evas_Object *genlist = NULL;
	Evas_Object *circle_genlist = NULL;
	const char *style = menu_its[id->index];

	/* Create item class */
	Elm_Genlist_Item_Class *ttc = elm_genlist_item_class_new();
	Elm_Genlist_Item_Class *gtc = elm_genlist_item_class_new();
	Elm_Genlist_Item_Class *itc = elm_genlist_item_class_new();
	Elm_Genlist_Item_Class *ptc = elm_genlist_item_class_new();

	if (!strcmp(style, "title") || !strcmp(style, "title_with_groupindex"))
		itc->item_style = "2text";
	else if (!strcmp(style, "select_mode") || !strcmp(style, "List: Go to Top"))
		itc->item_style = "1text";
	else if (strstr(style, "List: Seamless Effect"))
		itc->item_style = "1text";
	else
		itc->item_style = style;

	if (!strcmp(style, "multiline"))
		itc->func.text_get = _gl_long_text_get;
	else
		itc->func.text_get = _gl_text_get;

	itc->func.content_get = _gl_icon_get;
	itc->func.del = _gl_del;

	if (!strcmp(style, "title")) {
		ttc->item_style = "title";
		ttc->func.text_get = _gl_title_text_get;
		ttc->func.content_get = _gl_icon_get;
		ttc->func.del = _gl_del;
	} else if (!strcmp(style, "title_with_groupindex")) {
		ttc->item_style = "title_with_groupindex";
		ttc->func.text_get = _gl_text_get;
		ttc->func.content_get = _gl_icon_get;
		ttc->func.del = _gl_del;

		gtc->item_style = "groupindex";
		gtc->func.text_get = _gl_text_get;
		gtc->func.content_get = _gl_icon_get;
		gtc->func.del = _gl_del;
	}

	if (!strcmp(style, "select_mode")) {
		gvd->layout = elm_layout_add(ad->nf);
		genlist = _create_select_mode_genlist(gvd);
		itc->func.content_get = _gl_check_get;
		evas_object_event_callback_add(genlist, EVAS_CALLBACK_DEL, _gl_ctxpopup_del_cb, (void *) gvd);
	} else
		genlist = elm_genlist_add(ad->nf);

	/* COMPRESS MODE
		Because genlist shows full window, compress mode should be used. */
	elm_genlist_mode_set(genlist, ELM_LIST_COMPRESS);

	elm_object_style_set(genlist, "focus_bg");
	ptc->item_style = "padding";

	if (!strstr(style, "multiline"))
		elm_genlist_homogeneous_set(genlist, EINA_TRUE);

	if (!strcmp(style, "List: Go to Top"))
	{
		elm_object_scroll_back_to_top_enabled_set(genlist, EINA_TRUE);
		elm_object_scroll_back_to_top_cb_set(genlist, _back_to_top_cb);
	}
	gvd->genlist = genlist;

	circle_genlist = eext_circle_object_genlist_add(genlist, ad->circle_surface);
	eext_circle_object_genlist_scroller_policy_set(circle_genlist, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
	eext_rotary_object_event_activated_set(circle_genlist, EINA_TRUE);

	if (strstr(style, "divider"))
		itc->func.content_get = _gl_divider_icon_get;

	if (strcmp(style, "title") && strcmp(style, "title_with_groupindex"))
		elm_genlist_item_append(genlist, ptc, NULL, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);

	for (index = 0; index < NUM_OF_ITEMS; index++) {
		item_data *id1 = calloc(sizeof(item_data), 1);
		if (id1)
		{
			id1->index = index;
			id1->gvd = gvd;

			if (!strcmp(style, "title")) {
				if (index == 0)
					item = elm_genlist_item_append(
							genlist,			// genlist object
							ttc,				// item class
							id1,				// data
							NULL,
							ELM_GENLIST_ITEM_GROUP,
							_gl_sel,
							NULL);
				else
					item = elm_genlist_item_append(
							genlist,			// genlist object
							itc,				// item class
							id1,				// data
							NULL,
							ELM_GENLIST_ITEM_NONE,
							_gl_sel,
							NULL);
			} else if (!strcmp(style, "title_with_groupindex")) {
				if (index == 0)
					item = elm_genlist_item_append(
							genlist,			// genlist object
							ttc,				// item class
							id1,				// data
							NULL,
							ELM_GENLIST_ITEM_GROUP,
							_gl_sel,
							NULL);
				else if (index == 1)
					item = elm_genlist_item_append(
							genlist,			// genlist object
							gtc,				// item class
							id1,				// data
							NULL,
							ELM_GENLIST_ITEM_GROUP,
							_gl_sel,
							NULL);
				else
					item = elm_genlist_item_append(
							genlist,			// genlist object
							itc,				// item class
							id1,				// data
							NULL,
							ELM_GENLIST_ITEM_NONE,
							_gl_sel,
							NULL);
			} else if (!strcmp(style, "select_mode"))
				item = elm_genlist_item_append(
						genlist,				// genlist object
						itc,					// item class
						id1,					// data
						NULL,
						ELM_GENLIST_ITEM_NONE,
						_gl_select_mode_sel,
						gvd);
			else
				item = elm_genlist_item_append(
						genlist,				// genlist object
						itc,					// item class
						id1,					// data
						NULL,
						ELM_GENLIST_ITEM_NONE,
						_gl_sel,
						NULL);
			id1->item = item;
		}

		if (!strcmp(itc->item_style, "groupindex"))
			elm_genlist_item_select_mode_set(item, ELM_OBJECT_SELECT_MODE_DISPLAY_ONLY);
	}

	elm_genlist_item_append(genlist, ptc, NULL, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);

	elm_genlist_item_class_free(ttc);
	elm_genlist_item_class_free(gtc);
	elm_genlist_item_class_free(itc);
	elm_genlist_item_class_free(ptc);


	if (!strcmp(style, "select_mode"))
		nf_it = elm_naviframe_item_push(ad->nf, NULL, NULL, NULL, gvd->layout, "empty");
	else if (!strcmp(style, "1text")) {
		nf_it = elm_naviframe_item_push(ad->nf, NULL, NULL, NULL, genlist, "empty");
		elm_naviframe_item_pop_cb_set(it, _nf_pop_cb, genlist);
	}
	else if (!strcmp(style, "List: Seamless Effect"))
		nf_it = elm_naviframe_item_push_from(gvd->ad->nf, NULL, NULL, NULL, gvd->genlist, "empty", id->perspective_obj);
	else if (!strcmp(style, "List: Seamless Effect From Button"))
	{
		nf_it = elm_naviframe_item_push_from(gvd->ad->nf, NULL, NULL, NULL, gvd->genlist, "empty", id->perspective_obj);
		id->perspective_obj = NULL;
	} else
		nf_it = elm_naviframe_item_push(ad->nf, NULL, NULL, NULL, genlist, "empty");

	elm_object_item_data_set(nf_it, circle_genlist);
}

void
genlist_cb(void *data, Evas_Object *obj, void *event_info)
{
	appdata_s *ad = (appdata_s *)data;
	genlist_view_data *gvd = NULL;
	item_data *id = NULL;
	Evas_Object *genlist = NULL;
	Evas_Object *circle_genlist;
	Elm_Object_Item *nf_it;

	int i = 0;

	if (!ad) return;

	gvd = (genlist_view_data *)calloc(1, sizeof(genlist_view_data));
	if (!gvd) return;
	gvd->ad = ad;

	genlist = elm_genlist_add(ad->nf);
	gvd->main_genlist = genlist;
	elm_genlist_mode_set(genlist, ELM_LIST_COMPRESS);
	evas_object_smart_callback_add(genlist, "selected", gl_selected_cb, NULL);

	circle_genlist = eext_circle_object_genlist_add(genlist, ad->circle_surface);
	eext_circle_object_genlist_scroller_policy_set(circle_genlist, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_AUTO);
	eext_rotary_object_event_activated_set(circle_genlist, EINA_TRUE);

	Elm_Genlist_Item_Class *itc = elm_genlist_item_class_new();
	Elm_Genlist_Item_Class *ttc = elm_genlist_item_class_new();
	Elm_Genlist_Item_Class *ptc = elm_genlist_item_class_new();

	ttc->item_style = "title";
	ttc->func.text_get = _gl_menu_title_text_get;
	ttc->func.del = _gl_menu_del;

	itc->item_style = "default";
	itc->func.text_get = _gl_menu_text_get;
	itc->func.del = _gl_menu_del;

	ptc->item_style = "padding";
	ptc->func.del = _gl_menu_del;

	elm_genlist_item_append(genlist, ttc, NULL, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);

	for (i = 0; menu_its[i]; i++) {
		id = calloc(sizeof(item_data), 1);
		if (id)
		{
			id->gvd = gvd;
			id->index = i;
			if (strstr(menu_its[i], "List: Seamless Effect"))
			{
				Elm_Genlist_Item_Class *seamless_itc = elm_genlist_item_class_new();
				if (!strcmp(menu_its[i], "List: Seamless Effect"))
				{
					seamless_itc->item_style = "1text.1icon";
					seamless_itc->func.content_get = __genlist_item_content_get_cb;
					seamless_itc->func.text_get = _gl_text_get;
				}
				else
				{
					seamless_itc->item_style = "full";
					seamless_itc->func.content_get = __genlist_item_seamless_content_get_cb;
				}
				seamless_itc->func.del = _gl_del;

				id->item = elm_genlist_item_append(
							genlist,
							seamless_itc,
							id,
							NULL,
							ELM_GENLIST_ITEM_NONE,
							_create_genlist,
							id);
				elm_genlist_item_class_free(seamless_itc);
				if (strcmp(menu_its[i], "List: Seamless Effect"))
				{
					elm_genlist_item_select_mode_set(id->item, ELM_OBJECT_SELECT_MODE_DISPLAY_ONLY);
				}
			} else
				id->item = elm_genlist_item_append(genlist, itc, id, NULL, ELM_GENLIST_ITEM_NONE, _create_genlist, id);
		}
	}

	elm_genlist_item_append(genlist, ptc, NULL, NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);

	elm_genlist_item_class_free(ttc);
	elm_genlist_item_class_free(itc);
	elm_genlist_item_class_free(ptc);

	evas_object_event_callback_add(genlist, EVAS_CALLBACK_DEL, __genlist_delete_cb, (void *) gvd);
	evas_object_smart_callback_add(genlist, "aligned,item,detected", __aligned_item_detected_cb, (void *) gvd);

	nf_it = elm_naviframe_item_push(ad->nf, NULL, NULL, NULL, genlist, "empty");
	elm_object_item_data_set(nf_it, circle_genlist);
}
